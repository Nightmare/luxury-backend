<?php

namespace app\controllers\api\v1;

use app\forms\Rental;
use app\models\Order;
use app\components\ApiController;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class OrderController
 * @package app\controllers\api\v1
 */
class OrderController extends ApiController
{

    /**
     * @inheritdoc
     */
    public $updateScenario = 'update';

    /**
     * @inheritdoc
     */
    public $createScenario = 'create';

    /**
     * @inheritdoc
     */
    public $modelClass = Order::class;

    /**
     * @return Rental|Order
     * @throws BadRequestHttpException
     */
    public function actionBook()
    {
        $rentalForm = new Rental();
        if ($rentalForm->load(\Yii::$app->getRequest()->getBodyParams(), '') && $rentalForm->validate()) {
            try {
                return $rentalForm->bookCars();
            } catch (\Exception $e) {
                throw new BadRequestHttpException('Cannot book the cars.', 0, $e);
            }
        }

        return $rentalForm;
    }

    /**
     * @param int $id
     * @throws BadRequestHttpException
     */
    public function actionDecline($id)
    {
        $order = $this->getModel($id);
        try {
            $order->cancel();
        } catch (\Exception $e) {
            throw new BadRequestHttpException($e->getMessage(), 0, $e);
        }
    }

    /**
     * @param int $id
     * @throws BadRequestHttpException
     */
    public function actionApprove($id)
    {
        $order = $this->getModel($id);
        try {
            $order->approve();
        } catch (\Exception $e) {
            throw new BadRequestHttpException($e->getMessage(), 0, $e);
        }
    }

    /**
     * @return \app\models\Order[]|array
     */
    public function actionUncompleted()
    {
        return Order::uncompleted();
    }

    /**
     * @param int $id
     * @return \app\models\Order[]|array
     */
    public function actionApplicants($id)
    {
        return $this->getModel($id)->getApplicantsOrdersForApprove();
    }

    /**
     * @param int $id
     * @return null|Order
     * @throws NotFoundHttpException
     */
    private function getModel($id)
    {
        if (null === $order = Order::findOne($id)) {
            throw new NotFoundHttpException("Object not found: $id");
        }
        return $order;
    }

}
