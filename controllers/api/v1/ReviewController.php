<?php

namespace app\controllers\api\v1;

use app\models;
use app\components\ApiController;
use yii\web\{NotFoundHttpException,BadRequestHttpException};

/**
 * Class ReviewController
 * @package app\controllers\api\v1
 */
class ReviewController extends ApiController
{

    /**
     * @inheritdoc
     */
    public $updateScenario = 'update';

    /**
     * @inheritdoc
     */
    public $createScenario = 'create';

    /**
     * @inheritdoc
     */
    public $modelClass = models\CarReview::class;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['update'], $actions['view']);
        return $actions;
    }

    /**
     * @param int $id
     * @throws BadRequestHttpException
     */
    public function actionDecline($id)
    {
        $review = $this->getModel($id);
        try {
            $review->decline();
        } catch (\Exception $e) {
            throw new BadRequestHttpException($e->getMessage(), 0, $e);
        }
    }

    /**
     * @param int $id
     * @throws BadRequestHttpException
     */
    public function actionApprove($id)
    {
        $review = $this->getModel($id);
        try {
            $review->verify();
        } catch (\Exception $e) {
            throw new BadRequestHttpException($e->getMessage(), 0, $e);
        }
    }


    /**
     * @param int $id
     * @return null|models\CarReview
     * @throws NotFoundHttpException
     */
    private function getModel($id)
    {
        if (null === $review = models\CarReview::findOne($id)) {
            throw new NotFoundHttpException("Object not found: $id");
        }
        return $review;
    }

}
