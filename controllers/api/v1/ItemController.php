<?php

namespace app\controllers\api\v1;

use app\components\actions\UpdateAction;
use app\models\Item;
use app\components\ApiController;

/**
 * Class ItemController
 * @package app\controllers\v1
 */
class ItemController extends ApiController
{

    /**
     * @inheritdoc
     */
    public $updateScenario = 'update';

    /**
     * @inheritdoc
     */
    public $createScenario = 'create';

    /**
     * @inheritdoc
     */
    public $modelClass = Item::class;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();
        $actions['assets'] = [
            'class' => UpdateAction::class,
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
            'scenario' => $this->updateScenario,
        ];
        return $actions;
    }

}
