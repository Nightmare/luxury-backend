<?php

namespace app\controllers\api\v1;

use app\models;
use app\components\ApiController;

/**
 * Class TextController
 * @package app\controllers\api\v1
 */
class TextController extends ApiController
{

    /**
     * @inheritdoc
     */
    public $modelClass = models\Text::class;

}
