<?php

namespace app\controllers\api\v1;

use app\models;
use app\components\ApiController;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class ItemTemplateController
 * @package app\controllers\api\v1
 */
class ItemTemplateController extends ApiController
{

    /**
     * @inheritdoc
     */
    public $modelClass = models\ItemTemplate::class;

    /**
     * @param int $id
     * @param string $entity_type
     * @param int $entity_id
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionEntity(int $id, string $entity_type, int $entity_id)
    {
        switch ($entity_type) {
            case 'item':
                $instance = models\Item::findOne($entity_id);
                $table = 'item_view_template';
                break;

            case 'car':
                $instance = models\Car::findOne($entity_id);
                $table = 'car_template';
                break;

            case 'garage':
                $instance = models\Garage::findOne($entity_id);
                $table = 'garage_template';
                break;

            default:
                throw new BadRequestHttpException("Invalid entity type: $entity_type");
                break;
        }

        if (null === $instance) {
            throw new NotFoundHttpException("Entity not found: $entity_id");
        }

        if (null === $itemTemplate = models\ItemTemplate::findOne($id)) {
            throw new NotFoundHttpException("Item template not found: $id");
        }

        $sql = \Yii::$app->getDb()->getQueryBuilder()->batchInsert(
            "{{%$table}}",
            [$entity_type . '_id', 'item_template_id'],
            [[$instance->id, $id]]
        );

        \Yii::$app->getDb()->createCommand($sql)->execute();

        \Yii::$app->getResponse()->setStatusCode(201);
    }

}
