<?php

namespace app\controllers\api\v1;

use app\components\actions\UpdateAction;
use app\components\ApiController;
use app\models\Domain;
use yii\web\NotFoundHttpException;

/**
 * Class DomainController
 * @package app\controllers\v1
 */
class DomainController extends ApiController
{

    /**
     * @inheritdoc
     */
    public $updateScenario = 'update';

    /**
     * @inheritdoc
     */
    public $createScenario = 'create';

    /**
     * @inheritdoc
     */
    public $modelClass = Domain::class;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();
        $actions['assets'] = [
            'class' => UpdateAction::class,
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
            'scenario' => $this->updateScenario,
        ];
        return $actions;
    }

    /**
     * @param $domain
     * @return Domain
     * @throws NotFoundHttpException
     */
    public function actionDomain($domain)
    {
        if (null === $instance = Domain::findByDomain($domain)) {
            throw new NotFoundHttpException("Domain not found: $domain");
        }

        return $instance;
    }
}
