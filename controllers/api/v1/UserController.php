<?php

namespace app\controllers\api\v1;

use Yii;
use app\forms\{
    Login as LoginForm,
    Register as RegisterForm
};
use app\components\ApiController;
use yii\web\Response;
use yii\web\ServerErrorHttpException;
use app\models\{Auth, User};
use yii\authclient\{
    AuthAction, ClientInterface, clients\Facebook, clients\Twitter, clients\VKontakte
};

/**
 * Class UserController
 * @package app\controllers\v1
 */
class UserController extends ApiController
{

    /**
     * @inheritdoc
     */
    public $modelClass = User::class;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['except'] = ['login', 'register', 'current'];
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();
        $actions['auth'] = [
            'class' => AuthAction::class,
            'successCallback' => [$this, 'successCallback'],
        ];
        return $actions;
    }

    /**
     * Sign in user
     *
     * @return LoginForm|array|User
     * @throws \yii\base\InvalidConfigException
     */
    public function actionLogin()
    {
        $form = new LoginForm();

        if ($form->load(Yii::$app->getRequest()->getBodyParams(), '') && $form->login()) {
            /** @var User $user */
            $user = Yii::$app->getUser()->getIdentity();
            return $user;
        }

        return $form;
    }

    /**
     * Sign up user
     *
     * @return RegisterForm|null|\yii\web\IdentityInterface|array
     * @throws \yii\base\InvalidConfigException
     */
    public function actionRegister()
    {
        $form = new RegisterForm();
        if ($form->load(Yii::$app->getRequest()->getBodyParams(), '') && $form->register()) {
            $userService = Yii::$app->getUser();
            if ($userService->login($form->getUser(), User::COOKIE_EXPIRE_TIME)) {
                /** @var User $user */
                $user = $userService->getIdentity();
                $user->refresh();
                return $user;
            }
            return $form->getUser()->getErrors();
        }

        return $form;
    }

    /**
     * Ger logged in user
     *
     * @return \yii\web\IdentityInterface|User|array
     */
    public function actionMe()
    {
        /** @var User $user */
        $user = Yii::$app->getUser()->getIdentity();
        return $user ?: [];
    }

    /**
     * @return User
     * @throws ServerErrorHttpException
     */
    public function actionLogout()
    {
        /** @var User $user */
        $user = Yii::$app->getUser()->getIdentity();
        if (!Yii::$app->getUser()->logout()) {
            throw new ServerErrorHttpException('Cannot logout user.');
        }
        return $user;
    }

    /**
     * @param ClientInterface $client
     * @return Response
     * @throws \yii\db\Exception
     */
    public function successCallback(ClientInterface $client)
    {
        $userService = Yii::$app->getUser();
        $attributes = $client->getUserAttributes();
        $request = Yii::$app->getRequest();

        $userData = [
            'auth_type' => $request->get('authclient')
        ];
        $id = $attributes['id'] ?? null;
        if ($client instanceof VKontakte) {
            $name = implode(' ', [$attributes['first_name'] ?? '' , $attributes['last_name'] ?? '']);
            try {
                $birthday = (new \DateTime($attributes['bdate']))->format('Y-m-d');
            } catch (\Exception $e) {
                $birthday = null;
            }

            $userData += [
                'username' => $attributes['screen_name'] ?? null,
                'name' => trim($name) ?: null,
                'gender' => [null, User::GENDER_FEMALE, User::GENDER_MALE][$attributes['sex'] ?? 0] ?? null,
                'birthday' => $birthday,
            ];
        } elseif ($client instanceof Twitter) {
            $userData += [
                'name' => $attributes['name'] ?? null,
                'username' => $attributes['screen_name'] ?? null,
            ];
        } elseif ($client instanceof Facebook) {
            $userData += [
                'name' => $attributes['name'] ?? null,
                'email' => $attributes['email'] ?? null,
            ];
        } else {
            return $this->redirect(Yii::$app->getRequest()->get('back_url', '/'));
        }

        /** @var Auth $auth */
        $auth = Auth::find()->where([
            'source' => $client->getId(),
            'source_id' => $id,
        ])->one();

        if ($userService->getIsGuest()) {
            if ($auth) {
                /** @var User $user */
                $user = $auth->getUser()->one();
                $userService->login($user, User::COOKIE_EXPIRE_TIME);
            } else {
                $query = User::find();
                if (isset($userData['email'])) {
                    $query->orWhere(['email' => $userData['email'], 'auth_type' => $userData['auth_type']]);
                }

                if (isset($userData['username'])) {
                    $query->orWhere(['username' => $userData['username'], 'auth_type' => $userData['auth_type']]);
                }

                if ($query->exists()) {
//                    Yii::$app->getSession()->setFlash('error', [
//                        Yii::t('app', "User with the same email as in {client} account already exists but isn't linked to it. Login using email first to link it.", ['client' => $this->client->getTitle()]),
//                    ]);
                } else {
                    $user = new User($userData + ['password' => Yii::$app->getSecurity()->generateRandomString(6)]);

                    $transaction = User::getDb()->beginTransaction();
                    if ($user->save()) {
                        $auth = new Auth([
                            'user_id' => $user->getId(),
                            'source' => $client->getId(),
                            'source_id' => (string) $id,
                        ]);

                        if ($auth->save()) {
                            $transaction->commit();
                            $userService->login($user, User::COOKIE_EXPIRE_TIME);
                        }
                    }
                }
            }
        } else {
            if (!$auth) {
                (new Auth([
                    'user_id' => $userService->getId(),
                    'source' => $client->getId(),
                    'source_id' => (string) $id,
                ]))->save();
            }
        }

        return $this->redirect($request->get('back_url', '/'));
    }

}
