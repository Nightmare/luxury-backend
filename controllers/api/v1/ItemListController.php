<?php

namespace app\controllers\api\v1;

use app\components\actions\UpdateAction;
use app\models\Item;
use app\models\ItemList;
use app\components\ApiController;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

/**
 * Class ItemListController
 * @package app\controllers\v1
 */
class ItemListController extends ApiController
{

    /**
     * @inheritdoc
     */
    public $updateScenario = 'update';

    /**
     * @inheritdoc
     */
    public $createScenario = 'create';

    /**
     * @inheritdoc
     */
    public $modelClass = ItemList::class;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();
        $actions['assets'] = [
            'class' => UpdateAction::class,
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
            'scenario' => $this->updateScenario,
        ];
        return $actions;
    }

    public function actionItems($id)
    {
        /** @var ItemList $model */
        $model = $this->runAction('view', ['id' => $id]);
        return \Yii::createObject([
            'class' => ActiveDataProvider::class,
            'query' => Item::find()->where(['item_list_id' => $model['id']]),
        ]);
    }

    public function actionItem($id, $item_id)
    {
        /** @var ItemList $model */
        $model = $this->runAction('view', ['id' => $id]);
        if (null === $instance = Item::find()
                ->where(['item_list_id' => $model['id']])
                ->andWhere('id = :id OR slug = :id', [':id' => $item_id])->one()
        ) {
            throw new NotFoundHttpException("Item not found: $item_id");
        }

        return $instance;
    }
}
