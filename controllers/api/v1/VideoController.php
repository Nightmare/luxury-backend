<?php

namespace app\controllers\api\v1;

use app\components\ApiController;
use app\models;

/**
 * Class VideoController
 * @package app\controllers\api\v1
 */
class VideoController extends ApiController
{

    /**
     * @inheritdoc
     */
    public $modelClass = models\Video::class;

}
