<?php

namespace app\controllers\api\v1;

use app\models;
use app\components\ApiController;
use app\components\actions\UpdateAction;

/**
 * Class ImageController
 * @package app\controllers\api\v1
 */
class ImageController extends ApiController
{

    /**
     * @inheritdoc
     */
    public $updateScenario = 'update';

    /**
     * @inheritdoc
     */
    public $createScenario = 'create';

    /**
     * @inheritdoc
     */
    public $modelClass = models\Image::class;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();
        $actions['assets'] = [
            'class' => UpdateAction::class,
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
            'scenario' => $this->updateScenario,
        ];
        return $actions;
    }

}
