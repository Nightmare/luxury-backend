<?php

namespace app\controllers;

use Yii;;
use yii\web\{Controller, Response, HttpException};
use yii\base\{Exception, UserException};

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends Controller
{

    /**
     * @inheritdoc
     */
    public $layout = false;

    /**
     * @return array
     */
    public function actionError()
    {
        if (($exception = Yii::$app->getErrorHandler()->exception) === null) {
            // action has been invoked not from error handler, but by direct route, so we display '404 Not Found'
            $exception = new HttpException(404, Yii::t('yii', 'Page not found.'));
        }

        Yii::$app->getResponse()->format = Response::FORMAT_JSON;
        return [
            'code' => $exception instanceof HttpException ? $exception->statusCode : $exception->getCode(),
            'name' => $exception instanceof Exception ? $exception->getName() : Yii::t('yii', 'Error'),
            'message' => $exception instanceof UserException ? $exception->getMessage() : Yii::t('yii', 'An internal server error occurred.'),
        ];
    }

    /**
     * @return array
     */
    public function actionIndex()
    {
        Yii::$app->getResponse()->format = Response::FORMAT_JSON;
        return [
            'version' => Yii::$app->params['version'],
        ];
    }

}
