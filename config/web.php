<?php

use app\models\User;
use app\components\JsonVpResponseFormatter;
use yii\swiftmailer\Mailer;
use yii\web\{
    UrlManager,
    JsonParser
};
use yii\authclient\{
    Collection,
    clients\Facebook,
    clients\VKontakte,
    clients\Twitter
};
use yii\log\FileTarget;

Yii::setAlias('@static', getenv('FRONTEND_PATH'));

$params = require __DIR__ . DIRECTORY_SEPARATOR . 'params.php';
$config = [
    'id' => 'luxury',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            'enableCsrfValidation' => false,
            'enableCookieValidation' => true,
            'cookieValidationKey' => '0_tWPwSE-qAMnKltyRy0F5MZuLo2SlBt',
            'parsers' => [
                'application/json' => JsonParser::class,
            ],
        ],
        'response' => [
            'formatters' => [
                'jsonvp' => JsonVpResponseFormatter::class,
            ],
            'charset' => 'UTF-8',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'session' => [
            'name' => 'LUXURY_SESSION_ID',
            'cookieParams' => [
                'httpOnly' => true,
                'secure' => !YII_DEBUG,
            ],
        ],
        'user' => [
            'identityClass' => User::class,
            'enableAutoLogin' => true,
            'loginUrl' => null,
            'identityCookie' => [
                'name' => 'luxury_identity',
                'httpOnly' => true,
                'secure' => !YII_DEBUG,
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => Mailer::class,
            'useFileTransport' => true,
        ],
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
//            'defaultRoles' => [User::getRoles()],
            'defaultRoles' => ['admin', 'user'],
            'itemFile' => '@app/components/rbac/data/items.php',
            'assignmentFile' => '@app/components/rbac/data/assignments.php',
            'ruleFile' => '@app/components/rbac/data/rules.php',
        ],
        'authClientCollection' => [
            'class' => Collection::class,
            'clients' => [
                'facebook' => [
                    'class' => Facebook::class,
                    'clientId' => '749291078539745',
                    'clientSecret' => '6de4459c3eb808e991ce6d3cb5cd8770',
                ],
                'twitter' => [
                    'class' => Twitter::class,
                    'requestEmail' => 'true',
                    'consumerKey' => 'pVPyoXYqk2OjOwNWdEf86V3K0',
                    'consumerSecret' => 'GVV1Bv9dEBCeE4J1DKiQSsb4xFgw6khj0zZzu45ckeTRwV6uZV',
                ],
                'vkontakte' => [
                    'class' => VKontakte::class,
                    'clientId' => '5393193',
                    'clientSecret' => '2ziRGrMmcm3c5QUX9vPb',
                ],
            ],
        ],
        'log' => [
            'traceLevel' => 3,
            'targets' => [
                [
                    'class' => FileTarget::class,
                    'categories' => ['yii\db\Command::query', 'yii\db\Command::execute'],
                    'levels' => ['error', 'warning', 'info'],
                ],
            ],
        ],
        'urlManager' => [
            'class' => UrlManager::class,
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => require __DIR__ . DIRECTORY_SEPARATOR . 'url_rules.php',
        ],
        'db' => require __DIR__ . DIRECTORY_SEPARATOR . 'db.php',
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
