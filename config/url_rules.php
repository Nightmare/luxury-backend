<?php

use yii\rest\UrlRule;

return [
    '' => 'site/index',
    '<action:\w+>' => 'site/<action>',
    [
        'class' => UrlRule::class,
        'controller' => 'api/v1/user',
        'extraPatterns' => [
            'POST,OPTIONS login' => 'login',
            'POST,OPTIONS register' => 'register',
            'POST,OPTIONS logout' => 'logout',
            'GET,OPTIONS me' => 'me',
            'GET,OPTIONS auth' => 'auth',
        ],
    ],
    [
        'class' => UrlRule::class,
        'controller' => ['api/v1/video', 'api/v1/text'],
    ],
    [
        'class' => UrlRule::class,
        'controller' => ['api/v1/item-template'],
        'extraPatterns' => [
            'GET,OPTIONS <domain:(?:[-A-Za-z0-9]+\.)+[A-Za-z]{2,8}>' => 'domain',
            'POST,OPTIONS {id}/entity/<entity_type:(car|garage|item)>/<entity_id:\d+>' => 'entity',
        ],
    ],
    [
        'class' => UrlRule::class,
        'extraPatterns' => [
            'GET,OPTIONS <domain:(?:[-A-Za-z0-9]+\.)+[A-Za-z]{2,8}>' => 'domain',
            'POST,OPTIONS {id}/assets' => 'assets',
        ],
        'controller' => 'api/v1/domain',
    ],
    [
        'class' => UrlRule::class,
        'extraPatterns' => [
            'POST,OPTIONS {id}/assets' => 'assets',
        ],
        'controller' => 'api/v1/image',
    ],
    [
        'class' => UrlRule::class,
        'tokens' => [
            '{id}' => '<id:\d[\d,]*|[a-z0-9]+(?:-[a-z0-9]+)*>'
        ],
        'extraPatterns' => [
            'POST,OPTIONS {id}/assets' => 'assets',
        ],
        'controller' => ['api/v1/car', 'api/v1/garage', 'api/v1/item'],
    ],
    [
        'class' => UrlRule::class,
        'tokens' => [
            '{id}' => '<id:\\d[\\d,]*|[a-z0-9]+(?:-[a-z0-9]+)*>'
        ],
        'extraPatterns' => [
            'POST,OPTIONS {id}/assets' => 'assets',
            'GET,OPTIONS {id}/items' => 'items',
            'GET,OPTIONS {id}/items/<item_id:\\d[\\d,]*|[a-z0-9]+(?:-[a-z0-9]+)*>' => 'item',
        ],
        'controller' => ['api/v1/item-list'],
    ],
    [
        'class' => UrlRule::class,
        'extraPatterns' => [
            'POST,OPTIONS book' => 'book',
            'GET,OPTIONS uncompleted' => 'uncompleted',
            'POST,OPTIONS <id:\d+>/approve' => 'approve',
            'POST,OPTIONS <id:\d+>/decline' => 'decline',
            'GET,OPTIONS <id:\d+>/applicants' => 'applicants',
        ],
        'controller' => 'api/v1/order',
    ],
    [
        'class' => UrlRule::class,
        'extraPatterns' => [
            'POST,OPTIONS <id:\d+>/approve' => 'approve',
            'POST,OPTIONS <id:\d+>/decline' => 'decline',
        ],
        'controller' => 'api/v1/review',
    ],
];
