<?php

namespace app\commands;

use app\models\Order;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Class OrderController
 * @package app\commands
 */
class OrderController extends Controller
{

    public function actionCheckExpired()
    {
        $count = Order::checkAndUpdateExpired();
        $this->stdout('Expired order items count: ' . $count, Console::FG_CYAN);
    }
}
