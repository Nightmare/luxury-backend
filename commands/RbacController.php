<?php
namespace app\commands;

use app\models\User;
use Yii;
use yii\console\Controller;
use app\components\rbac\UserRoleRule;

/**
 * Class RbacController
 * @package app\commands
 */
class RbacController extends Controller
{

    public function actionInit()
    {
        $authManager = Yii::$app->getAuthManager();
        try {
            $authManager->removeAll();
        } catch (\Exception $e) {}

        // create roles
        $guest = $authManager->createRole('guest');
        $user = $authManager->createRole(User::ROLE_USER);
        $admin = $authManager->createRole(User::ROLE_ADMIN);

        // create simple, based on action{$NAME} permissions
        $index  = $authManager->createPermission('index');
        $view   = $authManager->createPermission('view');
        $create = $authManager->createPermission('create');
        $update = $authManager->createPermission('update');
        $delete = $authManager->createPermission('delete');
        $error  = $authManager->createPermission('error');
        $me     = $authManager->createPermission('me');
        $register = $authManager->createPermission('register');
        $logout = $authManager->createPermission('logout');
        $entity = $authManager->createPermission('entity');
        $domain = $authManager->createPermission('domain');
        $login  = $authManager->createPermission('login');
        $book   = $authManager->createPermission('book');
        $items  = $authManager->createPermission('items');
        $item   = $authManager->createPermission('item');
        $assets = $authManager->createPermission('assets');
        $decline = $authManager->createPermission('decline');
        $approve = $authManager->createPermission('approve');
        $uncompleted = $authManager->createPermission('uncompleted');
        $applicants = $authManager->createPermission('applicants');

        // add permissions
        $authManager->add($index);
        $authManager->add($view);
        $authManager->add($create);
        $authManager->add($update);
        $authManager->add($delete);
        $authManager->add($error);
        $authManager->add($me);
        $authManager->add($register);
        $authManager->add($logout);
        $authManager->add($login);
        $authManager->add($book);
        $authManager->add($decline);
        $authManager->add($approve);
        $authManager->add($approve);
        $authManager->add($uncompleted);
        $authManager->add($applicants);
        $authManager->add($domain);
        $authManager->add($assets);
        $authManager->add($entity);
        $authManager->add($items);
        $authManager->add($item);

        // add rule, based on UserExt->role === $user->role
        $userRoleRule = new UserRoleRule();
        $authManager->add(new $userRoleRule);

        // add rule "UserRoleRule" in roles
        $guest->ruleName    = $userRoleRule->name;
        $user->ruleName     = $userRoleRule->name;
        $admin->ruleName    = $userRoleRule->name;

        // add roles
        $authManager->add($guest);
        $authManager->add($user);
        $authManager->add($admin);

        // guest
        $authManager->addChild($guest, $book);
        $authManager->addChild($guest, $error);
        $authManager->addChild($guest, $index);
        $authManager->addChild($guest, $view);
        $authManager->addChild($guest, $me);
        $authManager->addChild($guest, $register);
        $authManager->addChild($guest, $login);
        $authManager->addChild($guest, $domain);
        $authManager->addChild($guest, $items);
        $authManager->addChild($guest, $item);

        // user
        $authManager->addChild($user, $guest);
        $authManager->addChild($user, $logout);

        // admin
        $authManager->addChild($admin, $user);
        $authManager->addChild($admin, $create);
        $authManager->addChild($admin, $update);
        $authManager->addChild($admin, $delete);
        $authManager->addChild($admin, $assets);
        $authManager->addChild($admin, $decline);
        $authManager->addChild($admin, $approve);
        $authManager->addChild($admin, $uncompleted);
        $authManager->addChild($admin, $applicants);
        $authManager->addChild($admin, $entity);
    }

}