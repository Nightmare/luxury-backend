<?php

namespace app\forms;

use Yii;
use yii\base\Model;
use app\models\User;

/**
 * Class Register
 * @package app\forms
 */
class Register extends Model
{

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $repeatPassword;

    /**
     * @var string
     */
    public $role;

    /**
     * @var User
     */
    protected $user;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->user = Yii::createObject([
            'class' => User::class,
        ]);
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'name', 'password'], 'required'],
            ['password', 'string', 'min' => 6],
            [['email', 'name'], 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => User::class, 'message' => Yii::t('app', 'User with such email already exist.')],
            ['role', 'in', 'range' => [User::ROLE_USER, User::ROLE_ADMIN]],
            ['repeatPassword', 'compare', 'compareAttribute' => 'password'],
            ['repeatPassword', 'safe'],
        ];
    }

    /**
     * Registers a new user account.
     * @return bool
     */
    public function register()
    {
        if (!$this->validate()) {
            return false;
        }

        $this->user->email = $this->email;
        $this->user->password = $this->password;
        $this->user->role = $this->role;
        $this->user->name = $this->name;

        return $this->user->save();
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

}