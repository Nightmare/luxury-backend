<?php

namespace app\forms;

use yii\base\Model;
use app\models\{
    Car, ComingSoonInterface, Domain, Garage, Order, OrderItem, User
};

/**
 * Class Rental
 * @package app\forms
 */
class Rental extends Model
{

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $phone;

    /**
     * @var array
     */
    public $items;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'items'], 'required'],
            [['name', 'phone'], 'string'],
            ['phone', 'match', 'pattern' => '/^\d{10,15}$/'],
            [['name', 'phone'], 'filter', 'filter' => 'trim'],
            ['items', function ($attribute) {
                if (!$this->hasErrors()) {
                    try {
                        $this->validateOrderItems($this->$attribute);
                    } catch (\Exception $e) {
                        $this->addError($attribute, $e->getMessage());
                    }
                }
            }],
        ];
    }

    /**
     * @param array $items
     * @throws \Exception
     */
    public function validateOrderItems($items)
    {
        if (!is_array($items) || count($items) === 0) {
            throw new \Exception('Order items cannot be empty.');
        }

        $comparisonDate = new \DateTime('today - 1second');
        foreach ($items as $slug => $item) {
            if (!(is_array($item) && count($item) > 0) ||
                !isset($item['price'], $item['periods'], $item['type']) ||
                !in_array($item['type'], Domain::getTypes(), true) ||
                !(ctype_digit((string) $item['price'])) ||
                !(is_array($item['periods'])  && count($item['periods']) > 0)
            ) {
                throw new \Exception('Order item structure is inconsistency.');
            }

            if ($item['type'] === Domain::ITEM_TYPE_SINGLE) {
                $billableItem = Car::findOne(['slug' => $slug]);
            } else {
                $billableItem = Garage::findOne(['slug' => $slug]);
            }

            if (null === $billableItem) {
                throw new \Exception('Cannot find an billable item for order item.');
            }

            if ($billableItem instanceof ComingSoonInterface) {
                if ($billableItem->isComingSoon()) {
                    throw new \Exception('Car in coming soon period.');
                }
            }

            $priceForItem = 0;
            foreach ($item['periods'] as $period) {
                if (!ctype_digit((string) $period['days'])) {
                    throw new \Exception('Days in order items periods does not exist or invalid.');
                }

                try {
                    $startsAt = new \DateTime($period['starts_at']);
                    $endsAt = clone $startsAt;
                    if ($period['days'] > 1) {
                        $endsAt->modify('+' . ($period['days'] - 1) . 'days');
                    }
                } catch (\Exception $e) {
                    throw new \Exception('Starts date in order items periods is invalid.', 0, $e);
                }

                if ($startsAt <= $comparisonDate) {
                    throw new \Exception('Starts date in order items periods should be greater than current time.');
                }
                $priceForItem += $period['days'] * $billableItem->getPrice();

                if (!$billableItem->isAvailableForPeriod($startsAt, $endsAt)) {
                    throw new \Exception('Car is not available for this period.');
                }
            }

            if ((int) $priceForItem !== (int) $item['price']) {
                throw new \Exception('Price in order items is invalid.');
            }
        }
    }

    /**
     * @throws \Exception
     * @return Order
     */
    public function bookCars()
    {
        try {
            $this->validateOrderItems($this->items);
        } catch (\Exception $e) {
            throw new \Exception('Cannot book the billing items.', 0, $e);
        }

        $transaction = \Yii::$app->getDb()->beginTransaction();
        try {
            $user = User::findByPhone($this->phone);

            if ($user === null) {
                $user = new User();
                $user->phone = $this->phone;
                $user->name = $this->name;
                $user->role = User::ROLE_USER;
                $user->save();
            }

            $order = new Order();
            $order->user_id = $user->id;
            $order->save();

            foreach ($this->items as $slug => $orderItem) {
                if ($orderItem['type'] === Domain::ITEM_TYPE_SINGLE) {
                    $billableItem = Car::findOne(['slug' => $slug]);
                    $field = 'car_id';
                } else {
                    $billableItem = Garage::findOne(['slug' => $slug]);
                    $field = 'garage_id';
                }

                foreach ($orderItem['periods'] as $period) {
                    $item = new OrderItem();
                    $item->$field = $billableItem->id;
                    $item->order_id = $order->id;
                    $item->price = (int) $orderItem['price'];
                    $item->status = OrderItem::STATUS_IN_PROGRESS;
                    $item->begins_at = (new \DateTime($period['starts_at']))->format('Y-m-d');
                    $item->days_count = (int) $period['days'];
                    $item->save();
                }
            }
            $transaction->commit();
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        return $order;
    }

}