<?php

use yii\db\Migration;

class m170222_130452_add_fields_to_garage extends Migration
{
    public function up()
    {
        $this->addColumn('{{%garage}}', 'meta_title', $this->string(255)->defaultValue(null)->after('priority'));
        $this->addColumn('{{%garage}}', 'meta_keywords', $this->text()->defaultValue(null)->after('meta_title'));
        $this->addColumn('{{%garage}}', 'meta_description', $this->text()->defaultValue(null)->after('meta_keywords'));
    }

    public function down()
    {
        $this->dropColumn('{{%garage}}', 'meta_title');
        $this->dropColumn('{{%garage}}', 'meta_keywords');
        $this->dropColumn('{{%garage}}', 'meta_description');
    }
}
