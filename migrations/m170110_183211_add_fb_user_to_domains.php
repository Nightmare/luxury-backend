<?php

use yii\db\Migration;

/**
 * Handles adding fb_user to table `domains`.
 */
class m170110_183211_add_fb_user_to_domains extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%domain}}', 'fb_user_id', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%domain}}', 'fb_user_id');
    }
}
