<?php

use yii\db\Migration;

class m170221_202020_make_car_id_nullable_in_item_template extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%item_template}}', 'car_id', $this->integer(10)->unsigned()->defaultValue(null));
    }

    public function down()
    {
        $this->alterColumn('{{%item_template}}', 'car_id', $this->integer(10)->unsigned()->notNull());
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
