<?php

use yii\db\Migration;

/**
 * Handles the creation for table `auth`.
 */
class m160401_200908_create_auth extends Migration
{
    
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%auth}}', [
            'id' => $this->primaryKey(10)->unsigned()->notNull(),
            'user_id' => $this->integer(10)->unsigned()->notNull(),
            'source' => $this->string(255)->notNull(),
            'source_id' => $this->string(255)->notNull(),
        ]);

        $this->createIndex('idx-auth-user_id', '{{%auth}}', 'user_id');
        $this->addForeignKey(
            'fk-auth-user_id-user-id',
            '{{%auth}}', 'user_id',
            '{{%user}}', 'id',
            'CASCADE', 'RESTRICT'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%auth}}');
    }
}
