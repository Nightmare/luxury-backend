<?php

use yii\db\Migration;

class m170404_122206_add_domain_fields extends Migration
{
    public function up()
    {
        $this->addColumn('{{%domain}}', 'main_phone_number', $this->string(30)->notNull());
    }

    public function down()
    {
        $this->dropColumn('{{%domain}}', 'main_phone_number');
    }
}
