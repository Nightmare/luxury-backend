<?php

use yii\db\Migration;
use yii\db\Query;

class m170409_090303_migrate_garage_templates_data extends Migration
{
    public function up()
    {
        $itemTemplates = (new Query())
            ->select('id,garage_id')
            ->from('{{%item_template}}')
            ->where('garage_id IS NOT NULL')
            ->all();

        if (count($itemTemplates) > 0) {
            $this->batchInsert(
                '{{%garage_template}}',
                ['garage_id', 'item_template_id'],
                array_map(function ($itemTemplates) { return [$itemTemplates['garage_id'], $itemTemplates['id']]; }, $itemTemplates)
            );
        }
        $this->dropColumn('{{%item_template}}', 'garage_id');
    }

    public function down()
    {
        echo "m170409_090303_migrate_garage_templates_data cannot be reverted.\n";

        return false;
    }
}
