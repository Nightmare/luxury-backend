<?php

use yii\db\Migration;

/**
 * Handles the creation of table `item_view_template`.
 */
class m170407_150739_create_item_view_template_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%item_view_template}}', [
            'item_id' => $this->integer(10)->unsigned()->notNull(),
            'item_template_id' => $this->integer(10)->unsigned()->notNull(),
        ]);

        $this->addPrimaryKey('item_view_template-item_id-item_template_id', '{{%item_view_template}}', ['item_id', 'item_template_id']);
        $this->addForeignKey(
            'fk-item_view_template-item_id-item-id',
            '{{%item_view_template}}', 'item_id',
            '{{%item}}', 'id',
            'CASCADE', 'RESTRICT'
        );

        $this->addForeignKey(
            'fk-item_view_template-item_template_id-item_template-id',
            '{{%item_view_template}}', 'item_template_id',
            '{{%item_template}}', 'id',
            'CASCADE', 'RESTRICT'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%item_view_template}}');
    }
}
