<?php

use yii\db\Migration;

/**
 * Handles the creation of table `garage_cars`.
 */
class m170117_141225_create_garage_cars_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%garage_cars}}', [
            'car_id' => $this->integer(10)->unsigned()->notNull(),
            'garage_id' => $this->integer(10)->unsigned()->notNull(),
            'PRIMARY KEY(car_id, garage_id)',
        ]);

        $this->addForeignKey(
            'fk-garage_cars-car_id-car-id',
            '{{%garage_cars}}', 'car_id',
            '{{%car}}', 'id',
            'CASCADE', 'RESTRICT'
        );

        $this->addForeignKey(
            'fk-garage_cars-garage_id-garage-id',
            '{{%garage_cars}}', 'garage_id',
            '{{%garage}}', 'id',
            'CASCADE', 'RESTRICT'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%garage_cars}}');
    }
}
