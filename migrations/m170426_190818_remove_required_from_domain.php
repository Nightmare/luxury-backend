<?php

use yii\db\Migration;

class m170426_190818_remove_required_from_domain extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%item}}', 'title', $this->string(255)->defaultValue(null));
        $this->alterColumn('{{%garage}}', 'title', $this->string(255)->defaultValue(null));
        $this->alterColumn('{{%car}}', 'title', $this->string(255)->defaultValue(null));
    }

    public function down()
    {
        $this->alterColumn('{{%item}}', 'title', $this->string(255)->notNull());
        $this->alterColumn('{{%garage}}', 'title', $this->string(255)->notNull());
        $this->alterColumn('{{%car}}', 'title', $this->string(255)->notNull());
    }
}
