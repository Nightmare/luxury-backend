<?php

use yii\db\Migration;

/**
 * Handles the creation of table `item`.
 */
class m170407_105246_create_item_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%item}}', [
            'id' => $this->primaryKey(10)->unsigned()->notNull(),
            'item_list_id' => $this->integer(10)->unsigned()->notNull(),
            'photo' => $this->string(255)->notNull(),
            'slug' => $this->string(255)->notNull(),
            'title' => $this->string(255)->notNull(),
            'name' => $this->string(255)->notNull(),
            'text' => $this->text()->notNull(),

            'meta_title' => $this->string(255)->defaultValue(null),
            'meta_keywords' => $this->text()->defaultValue(null),
            'meta_description' => $this->text()->defaultValue(null),

            'is_active' => $this->boolean()->notNull(),
            'created_at' => $this->dateTime()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->dateTime()->defaultValue(null) . ' on update CURRENT_TIMESTAMP',
        ]);

        $this->addForeignKey(
            'fk-item-item_list_id-item_list-id',
            '{{%item}}', 'item_list_id',
            '{{%item_list}}', 'id',
            'CASCADE', 'RESTRICT'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%item}}');
    }
}
