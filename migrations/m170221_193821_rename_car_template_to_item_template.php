<?php

use yii\db\Migration;

class m170221_193821_rename_car_template_to_item_template extends Migration
{
    public function up()
    {
        $this->renameTable('{{%car_template}}', '{{%item_template}}');
    }

    public function down()
    {
        $this->renameTable('{{%item_template}}', '{{%car_template}}');
    }
}
