<?php

use yii\db\Migration;

class m170409_093344_add_foreign_key_item_template extends Migration
{
    public function up()
    {
        $this->createIndex('fk-item_template-type-type_id', '{{%item_template}}', ['type_id', 'type'], true);
    }

    public function down()
    {
        $this->dropIndex('fk-item_template-type-type_id', '{{%item_template}}');
    }
}
