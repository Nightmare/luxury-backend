<?php

use yii\db\Migration;

/**
 * Handles the creation of table `car_template`.
 */
class m170407_151551_create_car_template_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%car_template}}', [
            'car_id' => $this->integer(10)->unsigned()->notNull(),
            'item_template_id' => $this->integer(10)->unsigned()->notNull(),
        ]);

        $this->addPrimaryKey('car_template-car_id-item_template_id', '{{%car_template}}', ['car_id', 'item_template_id']);
        $this->addForeignKey(
            'fk-car_template-car_id-car-id-rel',
            '{{%car_template}}', 'car_id',
            '{{%car}}', 'id',
            'CASCADE', 'RESTRICT'
        );

        $this->addForeignKey(
            'fk-car_template-item_template_id-item_template-id-rel',
            '{{%car_template}}', 'item_template_id',
            '{{%item_template}}', 'id',
            'CASCADE', 'RESTRICT'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%car_template}}');
    }
}
