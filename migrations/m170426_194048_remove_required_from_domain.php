<?php

use yii\db\Migration;

class m170426_194048_remove_required_from_domain extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%domain}}', 'garage_photo', $this->string()->defaultValue(null));
    }

    public function down()
    {
        $this->alterColumn('{{%domain}}', 'garage_photo', $this->string()->notNull());
    }
}
