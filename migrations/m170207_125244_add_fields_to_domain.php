<?php

use yii\db\Migration;

class m170207_125244_add_fields_to_domain extends Migration
{
    public function up()
    {
        $this->addColumn('{{%domain}}', 'title', $this->string()->notNull()->after('id'));
        $this->addColumn('{{%domain}}', 'contact_image', $this->string()->notNull()->after('title'));
        $this->addColumn('{{%domain}}', 'front_asset_type', 'ENUM(\'image\', \'video\') NOT NULL AFTER `contact_image`');
        $this->addColumn('{{%domain}}', 'items_type', 'ENUM(\'cortege\', \'single\') DEFAULT \'single\' AFTER `front_asset_type`');
        $this->addColumn('{{%domain}}', 'front_image', $this->string()->defaultValue(null)->after('items_type'));
        $this->addColumn('{{%domain}}', 'front_video', $this->string()->defaultValue(null)->after('front_image'));
    }

    public function down()
    {
        $this->dropColumn('{{%domain}}', 'title');
        $this->dropColumn('{{%domain}}', 'contact_image');
        $this->dropColumn('{{%domain}}', 'front_image');
        $this->dropColumn('{{%domain}}', 'front_video');
        $this->dropColumn('{{%domain}}', 'front_asset_type');
        $this->dropColumn('{{%domain}}', 'items_type');
    }
}
