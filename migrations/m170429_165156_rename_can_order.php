<?php

use yii\db\Migration;

class m170429_165156_rename_can_order extends Migration
{
    public function up()
    {
        $this->renameColumn('{{%domain}}', 'can_order', 'can_car_order');
        $this->addColumn('{{%domain}}', 'can_garage_order', $this->boolean()->defaultValue(true)->after('can_car_order'));
    }

    public function down()
    {
        $this->renameColumn('{{%domain}}', 'can_car_order', 'can_order');
        $this->dropColumn('{{%domain}}', 'can_garage_order');
    }
}
