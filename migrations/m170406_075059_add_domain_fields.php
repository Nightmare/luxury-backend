<?php

use yii\db\Migration;

class m170406_075059_add_domain_fields extends Migration
{
    public function up()
    {
        $this->addColumn('{{%domain}}', 'phone_numbers', $this->string(500)->defaultValue(null));
    }

    public function down()
    {
        $this->dropColumn('{{%domain}}', 'phone_numbers');
    }
}
