<?php

use yii\db\Migration;

class m170221_201650_add_fields_to_item_template extends Migration
{
    public function up()
    {
        $this->addColumn('{{%item_template}}', 'garage_id', $this->integer(10)->unsigned()->defaultValue(null)->after('car_id'));
        $this->addForeignKey(
            'fk-item_template-garage_id-garage-id',
            '{{%item_template}}', 'garage_id',
            '{{%garage}}', 'id',
            'CASCADE', 'RESTRICT'
        );
    }

    public function down()
    {
        $this->dropColumn('{{%item_template}}', 'garage_id');

    }
}
