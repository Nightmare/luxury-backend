<?php

use yii\db\Migration;

/**
 * Handles the creation of table `garage`.
 */
class m170117_140145_create_garage_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%garage}}', [
            'id' => $this->primaryKey(10)->unsigned()->notNull(),
            'name' => $this->string(50)->notNull(),
            'price' => $this->integer(10)->unsigned()->defaultValue(null),
            'slug' => $this->string(255)->notNull(),
            'priority' => $this->smallInteger()->unsigned()->defaultValue(0),
            'is_active' => $this->boolean()->notNull()->defaultValue(false),
            'created_at' => $this->dateTime()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->dateTime()->defaultValue(null) . ' on update CURRENT_TIMESTAMP',
        ]);
        $this->createIndex('idx-garage-slug', '{{%garage}}', ['slug'], true);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%garage}}');
    }
}
