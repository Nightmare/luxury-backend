<?php

use yii\db\Migration;

/**
 * Class m161017_172020_create_text
 */
class m161017_172020_create_text extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%text}}', [
            'id' => $this->primaryKey(10)->unsigned()->notNull(),
            'text' => $this->text()->notNull(),
            'created_at' => $this->dateTime()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->dateTime()->defaultValue(null) . ' on update CURRENT_TIMESTAMP',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%text}}');
    }
}
