<?php

use yii\db\Migration;

/**
 * Handles the creation for table `car_review`.
 */
class m161127_211620_create_car_review extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('car_review', [
            'id' => $this->primaryKey(10)->unsigned()->notNull(),
            'car_id' => $this->integer(10)->unsigned()->notNull(),
            'name' => $this->string(50)->notNull(),
            'email' => $this->string()->notNull(null),
            'review' => $this->string()->notNull(),
            'status' => 'ENUM(\'IN_VERIFICATION\', \'VERIFIED\', \'DECLINED\') NOT NULL DEFAULT \'IN_VERIFICATION\'',
            'created_at' => $this->dateTime()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->dateTime()->defaultValue(null) . ' on update CURRENT_TIMESTAMP',
        ]);
        $this->createIndex('idx-car_review-email', '{{%car_review}}', ['email']);

        $this->addForeignKey(
            'fk-car_review-car_id-car-id',
            '{{%car_review}}', 'car_id',
            '{{%car}}', 'id',
            'CASCADE', 'RESTRICT'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('car_review');
    }
}
