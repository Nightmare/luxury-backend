<?php

use yii\db\Migration;

class m170409_145308_add_assets_type_to_item_list extends Migration
{
    public function up()
    {
        $this->addColumn('{{%item_list}}', 'front_asset_type', 'ENUM(\'image\', \'video\') NOT NULL AFTER `domain_id`');
        $this->addColumn('{{%item_list}}', 'front_image', $this->string()->defaultValue(null)->after('front_asset_type'));
        $this->addColumn('{{%item_list}}', 'front_video', $this->string()->defaultValue(null)->after('front_image'));
    }

    public function down()
    {
        $this->dropColumn('{{%domain}}', 'front_image');
        $this->dropColumn('{{%domain}}', 'front_video');
        $this->dropColumn('{{%domain}}', 'front_asset_type');
    }
}
