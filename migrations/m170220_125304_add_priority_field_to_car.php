<?php

use yii\db\Migration;

class m170220_125304_add_priority_field_to_car extends Migration
{
    public function up()
    {
        $this->addColumn('{{%car}}', 'priority', $this->smallInteger()->unsigned()->defaultValue(0)->after('text'));
    }

    public function down()
    {
        $this->dropColumn('{{%car}}', 'priority');
    }
}
