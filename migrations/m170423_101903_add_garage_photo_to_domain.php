<?php

use yii\db\Migration;

class m170423_101903_add_garage_photo_to_domain extends Migration
{
    public function up()
    {
        $this->addColumn('{{%domain}}', 'garage_photo', $this->string()->notNull());

    }

    public function down()
    {
        $this->dropColumn('{{%domain}}', 'garage_photo');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
