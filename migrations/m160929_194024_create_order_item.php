<?php

use yii\db\Migration;

/**
 * Handles the creation for table `order_item`.
 */
class m160929_194024_create_order_item extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%order_item}}', [
            'id' => $this->primaryKey(10)->unsigned()->notNull(),
            'order_id' => $this->integer(10)->unsigned()->notNull(),
            'car_id' => $this->integer(10)->unsigned()->notNull(),
            'status' => 'ENUM(\'IN_PROGRESS\', \'FAILED\', \'COMPLETED\', \'CANCELLED\', \'EXPIRED\') NOT NULL DEFAULT \'IN_PROGRESS\'',
            'price' => $this->integer(10)->unsigned()->notNull(),
            'begins_at' => $this->date()->notNull(),
            'days_count' => $this->smallInteger()->notNull(),
            'created_at' => $this->dateTime()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->dateTime()->defaultValue(null) . ' on update CURRENT_TIMESTAMP',
        ]);

        $this->createIndex('idx-order_item-status-begins_at-days_count', '{{%order_item}}', ['status', 'begins_at', 'days_count']);

        $this->addForeignKey(
            'fk-order_item-order_id-order-id',
            '{{%order_item}}', 'order_id',
            '{{%order}}', 'id',
            'CASCADE', 'RESTRICT'
        );

        $this->addForeignKey(
            'fk-order_item-car_id-car-id',
            '{{%order_item}}', 'car_id',
            '{{%car}}', 'id',
            'CASCADE', 'RESTRICT'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%order_item}}');
    }
}
