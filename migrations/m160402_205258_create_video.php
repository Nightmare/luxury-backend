<?php

use yii\db\Migration;

/**
 * Handles the creation for table `video`.
 */
class m160402_205258_create_video extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%video}}', [
            'id' => $this->primaryKey(10)->unsigned()->notNull(),
            'url' => $this->string(255)->notNull(),
            'created_at' => $this->dateTime()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->dateTime()->defaultValue(null) . ' on update CURRENT_TIMESTAMP',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%video}}');
    }
}
