<?php

use yii\db\Migration;

class m170213_150455_add_domain_fields extends Migration
{
    public function up()
    {
        $this->addColumn('{{%domain}}', 'meta_description', $this->text()->defaultValue(null));
        $this->addColumn('{{%domain}}', 'meta_title', $this->string(255)->defaultValue(null));
        $this->addColumn('{{%domain}}', 'meta_keywords', $this->text()->defaultValue(null));
    }

    public function down()
    {
        $this->dropColumn('{{%domain}}', 'meta_description');
        $this->dropColumn('{{%domain}}', 'meta_title');
        $this->dropColumn('{{%domain}}', 'meta_keywords');
    }
}
