<?php

use yii\db\Migration;

class m170410_092828_add_can_order_to_domain extends Migration
{
    public function up()
    {
        $this->addColumn('{{%domain}}', 'can_order', $this->boolean()->defaultValue(true));
    }

    public function down()
    {
        $this->dropColumn('{{%domain}}', 'can_order');
    }
}
