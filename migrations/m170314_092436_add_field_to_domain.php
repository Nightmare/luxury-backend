<?php

use yii\db\Migration;

class m170314_092436_add_field_to_domain extends Migration
{
    public function up()
    {
        $this->addColumn('{{%domain}}', 'fb_link', $this->string(255)->defaultValue(null));
        $this->addColumn('{{%domain}}', 'instagram_link', $this->string(255)->defaultValue(null));
        $this->addColumn('{{%domain}}', 'vk_link', $this->string(255)->defaultValue(null));
    }

    public function down()
    {
        $this->dropColumn('{{%domain}}', 'fb_link');
        $this->dropColumn('{{%domain}}', 'instagram_link');
        $this->dropColumn('{{%domain}}', 'vk_link');
    }
}
