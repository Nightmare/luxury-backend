<?php

use yii\db\Migration;

class m170409_093647_drop_type_index_item_template extends Migration
{
    public function up()
    {
        $this->dropIndex('idx-car_template-type_id', '{{%item_template}}');
    }

    public function down()
    {
        $this->createIndex('idx-car_template-type_id', '{{%item_template}}', ['type']);
    }
}
