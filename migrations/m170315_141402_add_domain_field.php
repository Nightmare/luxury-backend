<?php

use yii\db\Migration;

class m170315_141402_add_domain_field extends Migration
{
    public function up()
    {
        $this->addColumn('{{%domain}}', 'performance_image', $this->string(255)->notNull());
    }

    public function down()
    {
        $this->dropColumn('{{%domain}}', 'performance_image');
    }
}
