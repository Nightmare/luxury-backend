<?php

use yii\db\Migration;

/**
 * Handles adding fb to table `domains`.
 */
class m170110_182757_add_fb_to_domains extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%domain}}', 'fb_app_id', $this->string(50));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%domain}}', 'fb_app_id');
    }
}
