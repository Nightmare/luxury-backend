<?php

use yii\db\Migration;

/**
 * Handles the creation of table `garage_template`.
 */
class m170409_084731_create_garage_template_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%garage_template}}', [
            'garage_id' => $this->integer(10)->unsigned()->notNull(),
            'item_template_id' => $this->integer(10)->unsigned()->notNull(),
        ]);

        $this->addPrimaryKey('garage_template-garage_id-item_template_id', '{{%garage_template}}', ['garage_id', 'item_template_id']);
        $this->addForeignKey(
            'fk-garage_template-garage_id-garage-id',
            '{{%garage_template}}', 'garage_id',
            '{{%garage}}', 'id',
            'CASCADE', 'RESTRICT'
        );

        $this->addForeignKey(
            'fk-garage_template-item_template_id-item_template-id',
            '{{%garage_template}}', 'item_template_id',
            '{{%item_template}}', 'id',
            'CASCADE', 'RESTRICT'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%garage_template}}');
    }
}
