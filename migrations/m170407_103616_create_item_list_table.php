<?php

use yii\db\Migration;

/**
 * Handles the creation of table `item`.
 */
class m170407_103616_create_item_list_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%item_list}}', [
            'id' => $this->primaryKey(10)->unsigned()->notNull(),
            'domain_id' => $this->integer(10)->unsigned()->notNull(),
            'slug' => $this->string(255)->notNull(),
            'title' => $this->string(255)->notNull(),
            'name' => $this->string(255)->notNull(),

            'meta_title' => $this->string(255)->defaultValue(null),
            'meta_keywords' => $this->text()->defaultValue(null),
            'meta_description' => $this->text()->defaultValue(null),

            'is_active' => $this->boolean()->notNull(),
            'created_at' => $this->dateTime()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->dateTime()->defaultValue(null) . ' on update CURRENT_TIMESTAMP',
        ]);

        $this->addForeignKey(
            'fk-item_list-domain_id-domain-id',
            '{{%item_list}}', 'domain_id',
            '{{%domain}}', 'id',
            'CASCADE', 'RESTRICT'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%item_list}}');
    }
}
