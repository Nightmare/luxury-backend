<?php

use yii\db\Migration;
use yii\db\Query;

class m170409_084950_migrate_item_templates_data extends Migration
{
    public function up()
    {
        $itemTemplates = (new Query())
            ->select('id,car_id')
            ->from('{{%item_template}}')
            ->where('car_id IS NOT NULL')
            ->all();

        if (count($itemTemplates) > 0) {
            $this->batchInsert(
                '{{%car_template}}',
                ['car_id', 'item_template_id'],
                array_map(function ($itemTemplates) { return [$itemTemplates['car_id'], $itemTemplates['id']]; }, $itemTemplates)
            );
        }
        $this->dropForeignKey('fk-car_template-car_id-car-id', '{{%item_template}}');
        $this->dropForeignKey('fk-item_template-garage_id-garage-id', '{{%item_template}}');
        $this->dropIndex('idx-car_template-car_id-type_id-type', '{{%item_template}}');
        $this->dropColumn('{{%item_template}}', 'car_id');
    }

    public function down()
    {
        echo "m170409_084950_migrate_item_templates_data cannot be reverted.\n";

        return false;
    }
}
