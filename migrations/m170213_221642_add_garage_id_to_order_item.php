<?php

use yii\db\Migration;

class m170213_221642_add_garage_id_to_order_item extends Migration
{
    public function up()
    {
        $this->addColumn('{{%order_item}}', 'garage_id', $this->integer(10)->unsigned()->defaultValue(null)->after('car_id'));
        $this->addForeignKey(
            'fk-order_item-garage_id-garage-id',
            '{{%order_item}}', 'garage_id',
            '{{%garage}}', 'id',
            'CASCADE', 'RESTRICT'
        );
    }

    public function down()
    {
        $this->dropColumn('{{%domain}}', 'garage_id');
    }
}
