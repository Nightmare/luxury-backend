<?php

use yii\db\Migration;

class m170417_100447_add_date_to_items extends Migration
{
    public function up()
    {
        $this->addColumn('{{%item}}', 'date', $this->date()->defaultValue(null)->after('is_active'));
    }

    public function down()
    {
        $this->dropColumn('{{%item}}', 'date');

    }
}
