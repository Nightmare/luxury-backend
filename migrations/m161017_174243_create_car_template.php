<?php

use yii\db\Migration;

/**
 * Handles the creation for table `car_template`.
 */
class m161017_174243_create_car_template extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%car_template}}', [
            'id' => $this->primaryKey(10)->unsigned()->notNull(),
            'car_id' => $this->integer(10)->unsigned()->notNull(),
            'type_id' => $this->integer(10)->unsigned()->notNull(),
            'type' => 'ENUM(\'image\', \'text\', \'video\') NOT NULL',
            'cols' => $this->smallInteger()->unsigned()->notNull()->comment('see grid system, max cols is 12 and min is 1'),
            'sort' => $this->smallInteger()->unsigned()->notNull(),
            'created_at' => $this->dateTime()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->dateTime()->defaultValue(null) . ' on update CURRENT_TIMESTAMP',
        ]);

        $this->createIndex('idx-car_template-order', '{{%car_template}}', 'sort');
        $this->createIndex('idx-car_template-type_id', '{{%car_template}}', 'type_id');
        $this->createIndex('idx-car_template-car_id-type_id-type', '{{%car_template}}', ['car_id', 'type_id', 'type'], true);

        $this->addForeignKey(
            'fk-car_template-car_id-car-id',
            '{{%car_template}}', 'car_id',
            '{{%car}}', 'id',
            'CASCADE', 'RESTRICT'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%car_template}}');
    }
}
