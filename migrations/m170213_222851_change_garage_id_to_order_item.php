<?php

use yii\db\Migration;

class m170213_222851_change_garage_id_to_order_item extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%order_item}}', 'car_id', $this->integer(10)->unsigned()->defaultValue(null));
    }

    public function down()
    {
        $this->alterColumn('{{%order_item}}', 'car_id', $this->integer(10)->unsigned()->notNull());
    }
}
