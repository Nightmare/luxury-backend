<?php

use yii\db\Migration;

/**
 * Handles the creation for table `user`.
 */
class m160401_194153_create_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(10)->unsigned()->notNull(),
            'email' => $this->string(255)->defaultValue(null),
            'username' => $this->string(255)->defaultValue(null),
            'phone' => $this->string(20)->defaultValue(null),
            'auth_type' => $this->string(50)->defaultValue(null),
            'birthday' => $this->date()->defaultValue(null),
            'gender' => 'ENUM(\'female\', \'male\') DEFAULT NULL',
            'role' => 'ENUM(\'admin\', \'user\') NOT NULL DEFAULT \'user\'',
            'name' => $this->string(255)->defaultValue(null),
            'auth_key' => $this->string(32)->notNull(),
            'access_token' => $this->string(40)->notNull(),
            'password_hash' => $this->string(255)->defaultValue(null),
            'created_at' => $this->dateTime()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->dateTime()->defaultValue(null) . ' on update CURRENT_TIMESTAMP',
        ]);

        $this->createIndex('idx-user-name', '{{%user}}', 'name');
        $this->createIndex('idx-user-phone-auth_type', '{{%user}}', ['phone', 'auth_type'], true);
        $this->createIndex('idx-user-email-auth_type', '{{%user}}', ['email', 'auth_type'], true);
        $this->createIndex('idx-user-username-auth_type', '{{%user}}', ['username', 'auth_type'], true);
        $this->createIndex('idx-user-access_token', '{{%user}}', 'access_token', true);

        $security = Yii::$app->getSecurity();
        $this->batchInsert('{{%user}}', ['email', 'role', 'name', 'auth_key', 'access_token', 'password_hash', 'gender'], [
            ['admin@luxury.md', 'admin', 'Ivan Zubok', $security->generateRandomString(32), $security->generateRandomString(40), $security->generatePasswordHash('counter16warik39119'), 'male'],
            ['orlov@luxury.md', 'admin', 'Alexandr Orlov', $security->generateRandomString(32), $security->generateRandomString(40), $security->generatePasswordHash('12QWasZX'), 'male'],
            ['alandi@luxury.md', 'admin', 'Alandi', $security->generateRandomString(32), $security->generateRandomString(40), $security->generatePasswordHash('12QWasZX'), 'male'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
