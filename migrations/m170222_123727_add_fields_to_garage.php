<?php

use yii\db\Migration;

class m170222_123727_add_fields_to_garage extends Migration
{
    public function up()
    {
        $this->addColumn('{{%garage}}', 'title', $this->string(255)->notNull()->after('name'));
        $this->addColumn('{{%garage}}', 'text', $this->text()->notNull()->after('title'));
    }

    public function down()
    {
        $this->dropColumn('{{%garage}}', 'title');
        $this->dropColumn('{{%garage}}', 'text');
    }
}
