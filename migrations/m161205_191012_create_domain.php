<?php

use yii\db\Migration;

/**
 * Handles the creation for table `domain`.
 */
class m161205_191012_create_domain extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%domain}}', [
            'id' => $this->primaryKey(10)->unsigned()->notNull(),
            'domain' => $this->string()->notNull(),
            'created_at' => $this->dateTime()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->dateTime()->defaultValue(null) . ' on update CURRENT_TIMESTAMP',
        ]);

        $this->createIndex('idx-domain-domain', '{{%domain}}', ['domain'], true);
        $this->batchInsert('{{%domain}}', ['id', 'domain'], [
            [1, 'luxury.md'],
            [2, 'alandi.md'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%domain}}');
    }
}
