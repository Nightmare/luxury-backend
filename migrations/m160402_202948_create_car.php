<?php

use yii\db\Migration;

/**
 * Handles the creation for table `car`.
 */
class m160402_202948_create_car extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%car}}', [
            'id' => $this->primaryKey(10)->unsigned()->notNull(),
            'photo' => $this->string(255)->notNull(),
            'price' => $this->integer(10)->unsigned()->defaultValue(null),
            'slug' => $this->string(255)->notNull(),
            'brand' => $this->string(122)->notNull(),
            'model' => $this->string(122)->notNull(),
            'year' => $this->smallInteger()->unsigned()->notNull(),
            'title' => $this->string(255)->notNull(),
            'text' => $this->text()->notNull(),

            'meta_title' => $this->string(255)->defaultValue(null),
            'meta_keywords' => $this->text()->defaultValue(null),
            'meta_description' => $this->text()->defaultValue(null),

//            'is_active' => $this->boolean()->notNull(),
            'created_at' => $this->dateTime()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->dateTime()->defaultValue(null) . ' on update CURRENT_TIMESTAMP',
        ]);

        $this->createIndex('idx-car-slug', '{{%car}}', ['slug'], true);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('car');
    }
}
