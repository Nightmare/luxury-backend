<?php

use yii\db\Migration;

class m170220_134419_add_fields_to_domain_menu extends Migration
{
    public function up()
    {
        $this->addColumn('{{%domain_menu}}', 'new_window', $this->boolean()->defaultValue(false)->after('url'));
    }

    public function down()
    {
        $this->dropColumn('{{%domain_menu}}', 'new_window');
    }
}
