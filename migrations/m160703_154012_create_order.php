<?php

use yii\db\Migration;

/**
 * Handles the creation for table `order`.
 */
class m160703_154012_create_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%order}}', [
            'id' => $this->primaryKey(10)->unsigned()->notNull(),
            'user_id' => $this->integer(10)->unsigned()->notNull(),
            'cancelled_id' => $this->integer(10)->unsigned()->defaultValue(null),
            'created_at' => $this->dateTime()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->dateTime()->defaultValue(null) . ' on update CURRENT_TIMESTAMP',
        ]);

        $this->addForeignKey(
            'fk-order-user_id-user-id',
            '{{%order}}', 'user_id',
            '{{%user}}', 'id',
            'CASCADE', 'RESTRICT'
        );

        $this->addForeignKey(
            'fk-order-cancelled_id-order-id',
            '{{%order}}', 'cancelled_id',
            '{{%order}}', 'id',
            'CASCADE', 'RESTRICT'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%order}}');
    }
}
