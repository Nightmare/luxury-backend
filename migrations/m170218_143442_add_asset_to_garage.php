<?php

use yii\db\Migration;

class m170218_143442_add_asset_to_garage extends Migration
{
    public function up()
    {
        $this->addColumn('{{%garage}}', 'photo', $this->string()->notNull()->after('name'));
    }

    public function down()
    {
        $this->dropColumn('{{%garage}}', 'photo');
    }
}
