<?php

use yii\db\Migration;

class m161205_200159_add_domain_id extends Migration
{
    public function up()
    {
        $this->addColumn('{{%car}}', 'domain_id', $this->integer(10)->unsigned()->notNull()->after('id'));
        $this->update('{{%car}}', ['domain_id' => 1], 'domain_id IS NULL OR domain_id = 0 OR domain_id = ""');

        $this->addForeignKey(
            'fk-car-domain_id-domain-id',
            '{{%car}}', 'domain_id',
            '{{%domain}}', 'id',
            'CASCADE', 'RESTRICT'
        );

    }

    public function down()
    {
        $this->dropColumn('{{%car}}', 'domain_id');
    }
}
