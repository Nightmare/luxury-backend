<?php

use yii\db\Migration;

class m170426_195244_remove_required_from_domain extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%item}}', 'text', $this->text()->defaultValue(null));
        $this->alterColumn('{{%garage}}', 'text', $this->text()->defaultValue(null));
        $this->alterColumn('{{%car}}', 'text', $this->text()->defaultValue(null));

        $this->alterColumn('{{%item}}', 'title', $this->string(255)->notNull());
        $this->alterColumn('{{%garage}}', 'title', $this->string(255)->notNull());
        $this->alterColumn('{{%car}}', 'title', $this->string(255)->notNull());
    }

    public function down()
    {
        $this->alterColumn('{{%item}}', 'text', $this->text()->notNull());
        $this->alterColumn('{{%garage}}', 'text', $this->text()->notNull());
        $this->alterColumn('{{%car}}', 'text', $this->text()->notNull());

        $this->alterColumn('{{%item}}', 'title', $this->string(255)->defaultValue(null));
        $this->alterColumn('{{%garage}}', 'title', $this->string(255)->defaultValue(null));
        $this->alterColumn('{{%car}}', 'title', $this->string(255)->defaultValue(null));
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
