<?php

use yii\db\Migration;

class m170219_225049_create_domain_menu extends Migration
{
    public function up()
    {
        $this->createTable('{{%domain_menu}}', [
            'id' => $this->primaryKey(10)->unsigned()->notNull(),
            'domain_id' => $this->integer(10)->unsigned()->notNull(),
            'title' => $this->string(50)->notNull(),
            'url' => $this->string()->notNull(),
            'created_at' => $this->dateTime()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->dateTime()->defaultValue(null) . ' on update CURRENT_TIMESTAMP',
        ]);
        $this->addForeignKey(
            'fk-domain_menu-domain_id-domain-id',
            '{{%domain_menu}}', 'domain_id',
            '{{%domain}}', 'id',
            'CASCADE', 'RESTRICT'
        );
    }

    public function down()
    {
        $this->dropTable('{{%domain_menu}}');
    }
}
