<?php

use yii\db\Migration;

class m170213_160612_add_domain_fields extends Migration
{
    public function up()
    {
        $this->addColumn('{{%domain}}', 'car_info_position', 'ENUM(\'left\', \'right\') DEFAULT  \'right\'');
    }

    public function down()
    {
        $this->dropColumn('{{%domain}}', 'car_info_position');
    }
}
