<?php

use yii\db\Migration;

class m170412_062938_add_show_in_front_page_to_item_list extends Migration
{
    public function up()
    {
        $this->addColumn('{{%item_list}}', 'show_in_front_page', $this->boolean()->defaultValue(false));
    }

    public function down()
    {
        $this->dropColumn('{{%item_list}}', 'show_in_front_page');
    }
}
