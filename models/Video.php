<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\models\Query\VideoQuery;

/**
 * This is the model class for table "{{%video}}".
 *
 * @property integer $id
 * @property string $url
 * @property string $created_at
 * @property string $updated_at
 */
class Video extends ActiveRecord implements AssetableInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%video}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['url'], 'string', 'max' => 255],
            [['url'], 'match', 'pattern' => '/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"\'>]+)/', 'message' => 'Invalid youtube video URL.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'url' => Yii::t('app', 'Url'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->url = static::getYoutubeEmbedUrl($this->url);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function export()
    {
        return $this->toArray(['id', 'url']);
    }

    /**
     * @inheritdoc
     * @return VideoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VideoQuery(get_called_class());
    }

    /**
     *
     * @param string $url
     *
     * @return string
     */
    public static function getYoutubeEmbedUrl($url)
    {
        preg_match('#^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&"\'>]+)#', $url, $matches);
        return 'https://www.youtube.com/embed/' . $matches[1];
    }
}
