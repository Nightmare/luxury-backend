<?php

namespace app\models;

use app\models\Query\OrderItemQuery;

/**
 * Interface BillableItemInterface
 * @package app\models
 */
interface BillableItemInterface
{

    /**
     * Returns true if item is available for dates period
     *
     * @param \DateTime $startsAt
     * @param \DateTime $endsAt
     *
     * @return bool
     */
    public function isAvailableForPeriod(\DateTime $startsAt, \DateTime $endsAt): bool;

    /**
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     * @return OrderItemQuery
     */
    public function getApplicantsOrderItems(\DateTime $dateFrom, \DateTime $dateTo): OrderItemQuery;

    /**
     * @return integer| null
     */
    public function getPrice();

    /**
     * @return OrderItemQuery
     */
    public function getOrderItems(): OrderItemQuery;

    /**
     * @return OrderItemQuery
     */
    public function getIntersectedOrderItems(): OrderItemQuery;

}