<?php

namespace app\models;

use app\models\Query\CarQuery;
use app\models\Query\GarageQuery;
use app\models\Query\OrderItemQuery;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use app\models\Query\OrderQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $cancelled_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Order $cancelled
 * @property OrderItem[] $orderItems
 * @property User $user
 */
class Order extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'cancelled_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['cancelled_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::class, 'targetAttribute' => ['cancelled_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'cancelled_id' => Yii::t('app', 'Cancelled ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return OrderQuery
     */
    public function getCancelledBy()
    {
        return $this->hasOne(Order::class, ['id' => 'cancelled_id']);
    }

    /**
     * @return OrderQuery
     */
    public function getCancelledOrders()
    {
        return $this->hasMany(Order::class, ['cancelled_id' => 'id']);
    }

    /**
     * @return OrderItemQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(OrderItem::class, ['order_id' => 'id']);
    }

    /**
     * @return CarQuery|ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::class, ['id' => 'car_id'])
            ->viaTable(OrderItem::tableName(), ['order_id' => 'id']);
    }

    /**
     * @return GarageQuery|ActiveQuery
     */
    public function getGarages()
    {
        return $this->hasMany(Garage::class, ['id' => 'garage_id'])
            ->viaTable(OrderItem::tableName(), ['order_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     */
    public function extraFields()
    {
        return array_merge(parent::extraFields(), ['user', 'cancelledBy', 'cancelledOrders', 'cars', 'garages', 'orderItems']);
    }

    /**
     * Cancels the order with order item
     *
     * @param Order $cancelledByOrder
     * @throws \Exception
     */
    public function cancel($cancelledByOrder = null)
    {
        /** @var OrderItem[] $orderItems */
        $orderItems = $this->getOrderItems()->all();
        foreach ($orderItems as $orderItem) {
            if (!$orderItem->canBeCancelled()) {
                throw new \Exception("Order item {$orderItem->id} can't be cancelled.");
            }
        }

        $transaction = $this::getDb()->beginTransaction();
        try {
            if ($cancelledByOrder instanceof Order) {
                $this->cancelled_id = $cancelledByOrder->id;
                $this->save(false);
            }
            OrderItem::updateAll(['status' => OrderItem::STATUS_CANCELLED], ['[[order_id]]' => $this->id]);
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * Approves the order with order item
     *
     * @throws \Exception
     */
    public function approve()
    {
        $transaction = $this::getDb()->beginTransaction();
        try {
            /** @var OrderItem[] $orderItems */
            $orderItems = $this->getOrderItems()->all();
            foreach ($orderItems as $orderItem) {
                if (!$orderItem->canBeCompleted()) {
                    throw new \Exception("Order item {$orderItem->id} can't be completed.");
                }
            }

            OrderItem::updateAll(['status' => OrderItem::STATUS_COMPLETED], ['[[order_id]]' => $this->id]);
            foreach ($this->getApplicantsOrdersForApprove() as $order) {
                $order->cancel($this);
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * @return Order[]
     */
    public function getApplicantsOrdersForApprove()
    {
        $orders = [];
        /** @var OrderItem[] $orderItems */
        $orderItems = $this->getOrderItems()->all();
        foreach ($orderItems as $orderItem) {
            foreach ($orderItem->getApplicantsOrderItemsForApprove() as $_orderItem) {
                if ($_orderItem->order_id !== $this->id && !array_key_exists($_orderItem->order_id, $orders)) {
                    $orders[$_orderItem->order_id] = $_orderItem->getOrder()->one();
                }
            }
        }

        return array_values($orders);
    }

    /**
     * @return array
     */
    public static function uncompleted()
    {
        $data = [];
        $db = Yii::$app->getDb();

        $orders = $db->createCommand(
            static::find()->uncompleted()->prepare($db->getQueryBuilder())->createCommand()->getRawSql()
        )->queryAll();

        foreach ($orders as $order) {
            $order['id'] = (int) $order['id'];
            $order['user_id'] = (int) $order['user_id'];
            $order['price'] = (int) $order['price'];
            $order['days_count'] = (int) $order['days_count'];
            $order['items'] = array_values(array_filter(
                array_merge(explode('|', $order['cars']), explode('|', $order['corteges']))
            ));
            unset($order['cars'], $order['corteges']);
            $data[] = $order;
        }
        return $data;
    }

    /**
     * @return int number of updated order items
     */
    public static function checkAndUpdateExpired()
    {
        $orderIds = ArrayHelper::getColumn(Order::find()->expired()->asArray()->all(), 'id');
        if (count($orderIds) === 0) {
            return 0;
        }
        return OrderItem::updateAll(['[[status]]' => OrderItem::STATUS_EXPIRED], ['[[order_id]]' => $orderIds]);
    }

    /**
     * @inheritdoc
     * @return \app\models\Query\OrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderQuery(get_called_class());
    }
}
