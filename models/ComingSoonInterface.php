<?php


namespace app\models;

interface ComingSoonInterface
{

    /**
     * @return bool
     */
    public function isComingSoon(): bool;

}