<?php

namespace app\models;

use app\models\Query\ItemTemplateQuery;
use yii\db\ActiveQuery;

/**
 * Interface ItemTemplateInterface
 * @package app\models
 */
interface ItemTemplateInterface
{
    /**
     * @return ItemTemplateQuery|ActiveQuery
     */
    public function getItemTemplates(): ItemTemplateQuery;
}