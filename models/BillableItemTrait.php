<?php

namespace app\models;

use app\models\Query\OrderItemQuery;

/**
 * Class BillableItemTrait
 * @method OrderItemQuery getIntersectedOrderItems()
 * @package app\models
 */
trait BillableItemTrait
{
    /**
     * @return OrderItemQuery
     */
    public function getUnavailablePeriods(): OrderItemQuery
    {
        return $this->getIntersectedOrderItems()->activeOrders()->periodsOnly()->orderByBeginsDate();
    }

    /**
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     * @return OrderItemQuery
     */
    public function getApplicantsOrderItems(\DateTime $dateFrom, \DateTime $dateTo): OrderItemQuery
    {
        return $this->getIntersectedOrderItems()->waitingNotExpiredItems()->forPeriod($dateFrom, $dateTo);
    }

    /**
     * @inheritdoc
     */
    public function isAvailableForPeriod(\DateTime $startsAt, \DateTime $endsAt): bool
    {
        return !$this->getIntersectedOrderItems()->completedItems()->forPeriod($startsAt, $endsAt)->exists();
    }
}