<?php

namespace app\models;

use app\components\validators\EitherValidator;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table '{{%user}}'.
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $auth_type
 * @property string $role
 * @property string $gender
 * @property string $birthday
 * @property string $password_hash
 * @property string $access_token
 * @property string $auth_key
 * @property string $created_at
 * @property string $updated_at
 */
class User extends ActiveRecord implements IdentityInterface
{

    const COOKIE_EXPIRE_TIME = 604800000; // 7000 days

    const ROLE_ADMIN = 'admin';
    const ROLE_USER = 'user';

    const GENDER_FEMALE = 'female';
    const GENDER_MALE = 'male';

    /**
     * @var string - plain password. Used for model validation.
     */
    public $password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'username'], 'filter', 'filter' => 'trim'],
            [['email', 'username', 'phone'], 'unique'],
            [['email'], 'email'],
            [['phone'], 'match', 'pattern' => '/^\d{10,15}$/'],
            [['role'], 'in', 'range' => static::getRoles()],
            [['gender'], 'in', 'range' => [self::GENDER_FEMALE, self::GENDER_MALE]],
            [['birthday', 'role', 'created_at', 'updated_at'], 'safe'],
            [['birthday'], 'date', 'format' => 'php:Y-m-d'],
            [['name', 'email', 'username', 'password_hash'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 20],
            [['access_token'], 'string', 'max' => 40],
            [['auth_type'], 'string', 'max' => 50],
            [['auth_key'], 'string', 'max' => 32],
            [['email', 'username', 'phone'], EitherValidator::class],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'role' => Yii::t('app', 'Role'),
            'gender' => Yii::t('app', 'Gender'),
            'birthday' => Yii::t('app', 'Birthday'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'access_token' => Yii::t('app', 'Access Token'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->getAttribute('auth_key');
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @param string $email - user email
     * @return null|static
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * @param string $phone - user phone
     * @return null|static
     */
    public static function findByPhone($phone)
    {
        return static::findOne(['phone' => $phone]);
    }

    /**
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->getAttribute('password_hash'));
    }

    /**
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function generatePasswordHash()
    {
        $this->setAttribute('password_hash', Yii::$app->getSecurity()->generatePasswordHash($this->password));

        return $this;
    }

    /**
     * Returns true if user is admin
     *
     * @return bool
     */
    public function isAdmin()
    {
        return $this->role === static::ROLE_ADMIN;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::class,
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => 'auth_key',
                ],
                'value' => Yii::$app->getSecurity()->generateRandomString(32),
            ],
            [
                'class' => AttributeBehavior::class,
                'attributes' => [
                    static::EVENT_BEFORE_INSERT => 'access_token',
                ],
                'value' => Yii::$app->getSecurity()->generateRandomString(40),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if (!$this->getIsNewRecord()) {
            return true;
        }

        if ($this->password) {
            $this->generatePasswordHash();
        }

        if (!$this->role) {
            $this->role = static::ROLE_USER;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        $fields = parent::fields();
        unset($fields['password_hash'], $fields['auth_key'], $fields['access_token']);
        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuths()
    {
        return $this->hasMany(Auth::class, ['user_id' => 'id']);
    }

    /**
     * @return array
     */
    public static function getRoles()
    {
        return [self::ROLE_USER, self::ROLE_ADMIN];
    }

}