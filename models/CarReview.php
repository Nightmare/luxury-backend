<?php

namespace app\models;

use Yii;
use yii\db\{ActiveRecord,ActiveQuery};
use app\models\Query\CarReviewQuery;
use himiklab\yii2\recaptcha\ReCaptchaValidator;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "{{%car_review}}".
 *
 * @property integer $id
 * @property integer $car_id
 * @property string $name
 * @property string $email
 * @property string $review
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Car $car
 */
class CarReview extends ActiveRecord
{

    const STATUS_IN_VERIFICATION = 'IN_VERIFICATION';
    const STATUS_VERIFIED = 'VERIFIED';
    const STATUS_DECLINED = 'DECLINED';

    /**
     * VIRTUAL FIELD
     * @var string
     */
    public $recaptcha;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%car_review}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_id', 'name', 'email', 'review'], 'required'],
            [['car_id'], 'integer'],
            [['!status'], 'string', 'on' => ['create']],
            [['email'], 'email'],
            [['status'], 'in', 'range' => [static::STATUS_IN_VERIFICATION, static::STATUS_VERIFIED, static::STATUS_DECLINED]],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 50],
            [['email', 'review'], 'string', 'max' => 255],
            [['name', 'review'], function ($attribute) {
                $this->$attribute = HtmlPurifier::process($this->$attribute);
            }],
            [['car_id'], 'exist', 'skipOnError' => true, 'targetClass' => Car::class, 'targetAttribute' => ['car_id' => 'id']],
            [['recaptcha'], ReCaptchaValidator::class, 'secret' => Yii::$app->params['googleSecretKey']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'car_id' => Yii::t('app', 'Car ID'),
            'name' => Yii::t('app', 'Name'),
            'email' => Yii::t('app', 'Email'),
            'review' => Yii::t('app', 'Review'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::class, ['id' => 'car_id']);
    }

    /**
     * @return bool
     */
    public function verify()
    {
        $this->status = static::STATUS_VERIFIED;
        return $this->save(false);
    }

    /**
     * @return bool
     */
    public function decline()
    {
        $this->status = static::STATUS_DECLINED;
        return $this->save(false);
    }

    /**
     * @inheritdoc
     * @return CarReviewQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CarReviewQuery(get_called_class());
    }
}
