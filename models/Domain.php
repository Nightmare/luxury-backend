<?php

namespace app\models;

use app\components\behaviors\UploadImageBehavior;
use app\components\validators\EitherValidator;
use app\models\Query\CarQuery;
use app\models\Query\DomainMenuQuery;
use app\models\Query\ItemListQuery;
use mongosoft\file\UploadBehavior;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use app\models\Query\DomainQuery;

/**
 * This is the model class for table "{{%domain}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $front_image
 * @property string $front_video
 * @property string $front_asset_type
 * @property string $items_type
 * @property string $domain
 * @property string $contact_image
 * @property string $created_at
 * @property string $updated_at
 * @property string $fb_app_id
 * @property string $fb_user_id
 * @property string $meta_description
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $car_info_position
 * @property string $fb_link
 * @property string $instagram_link
 * @property string $vk_link
 * @property string $performance_image
 * @property string $main_phone_number
 * @property string $phone_numbers
 * @property integer $can_car_order
 * @property integer $can_garage_order
 * @property string $garage_photo
 *
 * @property Car[] $cars
 * @property ItemList[] $itemLists
 */
class Domain extends ActiveRecord
{

    const FRONT_ASSET_TYPE_IMAGE = 'image';
    const FRONT_ASSET_TYPE_VIDEO = 'video';

    const ITEM_TYPE_CORTEGE = 'cortege';
    const ITEM_TYPE_SINGLE = 'single';

    const CAR_INFO_POSITION_LEFT = 'left';
    const CAR_INFO_POSITION_RIGHT = 'right';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%domain}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'front_asset_type', 'domain', 'contact_image', 'performance_image', 'main_phone_number'], 'required', 'on' => ['create']],
            [['front_asset_type'], 'in', 'range' => [self::FRONT_ASSET_TYPE_IMAGE, self::FRONT_ASSET_TYPE_VIDEO]],
            [['items_type'], 'in', 'range' => [self::ITEM_TYPE_CORTEGE, self::ITEM_TYPE_SINGLE]],
            [['car_info_position'], 'in', 'range' => [self::CAR_INFO_POSITION_LEFT, self::CAR_INFO_POSITION_RIGHT]],
            [['front_asset_type', 'items_type', 'fb_user_id', 'meta_description', 'meta_keywords'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'domain', 'meta_title', 'fb_link', 'instagram_link', 'vk_link'], 'string', 'max' => 255],
            [['fb_link', 'instagram_link', 'vk_link'], 'url'],
            [['can_car_order', 'can_garage_order'], 'integer'],
            [['fb_app_id'], 'string', 'max' => 50],
            [['phone_numbers'], 'string', 'max' => 500],
            [['main_phone_number'], 'string', 'max' => 30],
            [['main_phone_number', 'fb_user_id'], 'match', 'pattern' => '/^\d+(,\d+)*$/'],
            [['contact_image', 'front_image', 'performance_image', 'garage_photo'], 'image', 'extensions' => 'jpg, jpeg, png', 'on' => ['create', 'update']],
            [['front_video'], 'file', 'extensions' => 'mp4, 3gp, mov, m4v, mpeg, mspg', 'on' => ['create', 'update']],
            [['front_image', 'front_video'], EitherValidator::class, 'on' => ['create']],
            [['domain'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'front_image' => Yii::t('app', 'Front Image'),
            'garage_photo' => Yii::t('app', 'Garage Photo'),
            'front_video' => Yii::t('app', 'Front Video'),
            'front_asset_type' => Yii::t('app', 'Front Asset Type'),
            'items_type' => Yii::t('app', 'Items Type'),
            'domain' => Yii::t('app', 'Domain'),
            'contact_image' => Yii::t('app', 'Contact Image'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'fb_user_id' => Yii::t('app', 'Facebook Admin IDs'),
            'fb_app_id' => Yii::t('app', 'Facebook Application ID'),
            'meta_description' => Yii::t('app', 'Meta Description'),
            'meta_title' => Yii::t('app', 'Meta Title'),
            'meta_keywords' => Yii::t('app', 'Meta Keywords'),
            'car_info_position' => Yii::t('app', 'Car info position'),
            'fb_link' => Yii::t('app', 'Fb Link'),
            'instagram_link' => Yii::t('app', 'Instagram Link'),
            'vk_link' => Yii::t('app', 'Vk Link'),
            'performance_image' => Yii::t('app', 'Performance Image'),
            'main_phone_number' => Yii::t('app', 'Main Phone Number'),
            'phone_numbers' => Yii::t('app', 'Phone Numbers'),
            'can_car_order' => Yii::t('app', 'Can Car Order'),
            'can_garage_order' => Yii::t('app', 'Can Garage Order'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => UploadImageBehavior::class,
                'attribute' => 'garage_photo',
                'instanceByName' => true,
                'scenarios' => ['create', 'update'],
                'path' => '@static/uploads/domain/{id}',
                'url' => '/uploads/domain/{id}',
                'thumbs' => [],
            ],
            [
                'class' => UploadImageBehavior::class,
                'attribute' => 'contact_image',
                'instanceByName' => true,
                'scenarios' => ['create', 'update'],
                'path' => '@static/uploads/domain/{id}',
                'url' => '/uploads/domain/{id}',
                'thumbs' => [],
            ],
            [
                'class' => UploadImageBehavior::class,
                'attribute' => 'performance_image',
                'instanceByName' => true,
                'scenarios' => ['create', 'update'],
                'path' => '@static/uploads/domain/{id}',
                'url' => '/uploads/domain/{id}',
                'thumbs' => [],
            ],
            [
                'class' => UploadImageBehavior::class,
                'attribute' => 'front_image',
                'instanceByName' => true,
                'scenarios' => ['create', 'update'],
                'path' => '@static/uploads/domain/{id}',
                'url' => '/uploads/domain/{id}',
                'thumbs' => [],
            ],
            [
                'class' => UploadBehavior::class,
                'attribute' => 'front_video',
                'instanceByName' => true,
                'scenarios' => ['create', 'update'],
                'path' => '@static/uploads/domain/{id}',
                'url' => '/uploads/domain/{id}',
            ],
        ];
    }

    /**
     * @return CarQuery|ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::class, ['domain_id' => 'id']);
    }

    /**
     * @return ItemListQuery|ActiveQuery
     */
    public function getItemLists()
    {
        return $this->hasMany(ItemList::class, ['domain_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function extraFields()
    {
        return array_merge(parent::extraFields(), ['itemLists']);
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        /** @var UploadImageBehavior|self $self */
        $self = $this;
        $fields = parent::fields();
        foreach ($fields as $field) {
            if (in_array($field, ['contact_image', 'front_image', 'garage_photo', 'front_video', 'performance_image'], true)) {
                $fields[$field . '_url'] = function () use ($self, $field) {
                    return $self->getUploadUrl($field);
                };
            }
        }

        return $fields;
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [static::ITEM_TYPE_CORTEGE, static::ITEM_TYPE_SINGLE];
    }

    /**
     * @inheritdoc
     * @return DomainQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DomainQuery(get_called_class());
    }

    /**
     * @param string $domain
     * @return null|static
     */
    public static function findByDomain($domain)
    {
        return static::findOne(['domain' => $domain]);
    }
}
