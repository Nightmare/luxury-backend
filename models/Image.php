<?php

namespace app\models;

use app\models\Query\ImageQuery;
use Yii;
use yii\db\ActiveRecord;
use app\components\behaviors\UploadImageBehavior;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%image}}".
 *
 * @property integer $id
 * @property string $url
 * @property string $created_at
 * @property string $updated_at
 */
class Image extends ActiveRecord implements AssetableInterface
{

    /**
     * @var array|null
     */
    public $crop_area_bounds;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%image}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url'], 'required'],
            [['crop_area_bounds'], function ($attribute) {
                if ($attribute) {
                    try {
                        $value = Json::decode($this->$attribute);
                    } catch (\Exception $e) {
                        $value = null;
                    }

                    if (!is_array($value) || count($value) === 0) {
                        $this->addError($attribute, 'Invalid crop area bounds value.');
                    } else {
                        if ([] === array_diff_key(array_flip(['top', 'bottom', 'left', 'right']), $value)) {
                            if (!is_int($value['top']) ||
                                !is_int($value['left']) ||
                                !is_int($value['right']) ||
                                !is_int($value['bottom']) ||
                                $value['top'] < 0 ||
                                $value['left'] < 0 ||
                                $value['left'] >= $value['right'] ||
                                $value['bottom'] >= $value['top']
                            ) {
                                $this->addError($attribute, 'Invalid crop area bounds value.');
                            } else {
                                $this->$attribute = $value;
                            }
                        } else {
                            $this->addError($attribute, 'Invalid crop area bounds value.');
                        }
                    }
                }
            }],
            [['created_at', 'updated_at'], 'safe'],
            [['url'], 'image', 'extensions' => 'jpg, jpeg, png', 'on' => ['create', 'update']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => UploadImageBehavior::class,
                'attribute' => 'url',
                'cropAreaBoundsAttribute' => 'crop_area_bounds',
                'instanceByName' => true,
                'scenarios' => ['create', 'update'],
                'path' => '@static/uploads/image/{id}',
                'url' => '/uploads/image/{id}',
                'thumbs' => [],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'url' => Yii::t('app', 'Url'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function export()
    {
        return $this->toArray(['id', 'url']);
    }

    /**
     * @inheritdoc
     * @return ImageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ImageQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        /** @var UploadImageBehavior|self $self */
        $self = $this;
        $fields = parent::fields();
        $fields['url'] = function () use ($self) {
            return $self->getUploadUrl('url');
        };

        return $fields;
    }

}
