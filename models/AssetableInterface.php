<?php

namespace app\models;

/**
 * Interface AssetableInterface
 * @package app\models
 */
interface AssetableInterface
{

    /**
     * Returns exported array
     *
     * @return array
     */
    public function export();

}