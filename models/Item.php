<?php

namespace app\models;

use app\models\Query\ItemListQuery;
use app\models\Query\ItemTemplateQuery;
use Yii;
use app\components\behaviors\UploadImageBehavior;
use app\models\Query\ItemQuery;
use yii\behaviors\SluggableBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "{{%item}}".
 *
 * @property integer $id
 * @property integer $item_list_id
 * @property string $photo
 * @property string $slug
 * @property string $title
 * @property string $name
 * @property string $text
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property integer $is_active
 * @property string $date
 * @property string $created_at
 * @property string $updated_at
 *
 * @property ItemList $itemList
 */
class Item extends ActiveRecord
{
    use AssetableTrait;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%item}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_list_id', 'photo', 'slug', 'name', 'title', 'is_active'], 'required'],
            [['item_list_id', 'is_active'], 'integer'],
            [['text', 'meta_keywords', 'meta_description'], 'string'],
            [['date'], 'date', 'format' => 'php:Y-m-d'],
            [['text', 'title'], function ($attribute) {
                $this->$attribute = HtmlPurifier::process($this->$attribute);
            }],
            [['date', 'created_at', 'updated_at'], 'safe'],
            [['title', 'name', 'meta_title'], 'string', 'max' => 255],
            [['photo'], 'image', 'extensions' => 'jpg, jpeg, png', 'on' => ['create', 'update']],
            [['item_list_id'], 'exist', 'skipOnError' => true, 'targetClass' => ItemList::class, 'targetAttribute' => ['item_list_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'item_list_id' => Yii::t('app', 'Item List ID'),
            'photo' => Yii::t('app', 'Photo'),
            'slug' => Yii::t('app', 'Slug'),
            'title' => Yii::t('app', 'Title'),
            'name' => Yii::t('app', 'Name'),
            'text' => Yii::t('app', 'Text'),
            'meta_title' => Yii::t('app', 'Meta Title'),
            'meta_keywords' => Yii::t('app', 'Meta Keywords'),
            'meta_description' => Yii::t('app', 'Meta Description'),
            'is_active' => Yii::t('app', 'Is Active'),
            'date' => Yii::t('app', 'Date'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            SluggableBehavior::class => [
                'class' => SluggableBehavior::class,
                'attribute' => ['name'],
                'ensureUnique' => true,
            ],
            [
                'class' => UploadImageBehavior::class,
                'attribute' => 'photo',
                'instanceByName' => true,
                'scenarios' => ['create', 'update'],
                'path' => '@static/uploads/item/{id}/image',
                'url' => '/uploads/item/{id}/image',
                'thumbs' => [],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        /** @var UploadImageBehavior|self $self */
        $self = $this;
        $fields = parent::fields();
        $fields['photo'] = function () use ($self) {
            return $self->getUploadUrl('photo');
        };

        return $fields;
    }

    public function extraFields()
    {
        return array_merge(parent::extraFields(), ['itemList', 'assets']);
    }

    /**
     * @return ItemTemplateQuery|ActiveQuery
     */
    public function getItemTemplates(): ItemTemplateQuery
    {
        return $this->hasMany(ItemTemplate::class, ['id' => 'item_template_id'])
            ->viaTable('{{%item_view_template}}', ['item_id' => 'id']);
    }

    /**
     * @return ItemListQuery|ActiveQuery
     */
    public function getItemList()
    {
        return $this->hasOne(ItemList::class, ['id' => 'item_list_id']);
    }

    /**
     * @inheritdoc
     * @return ItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ItemQuery(get_called_class());
    }
}
