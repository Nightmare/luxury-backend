<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\models\Query\TextQuery;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "{{%text}}".
 *
 * @property integer $id
 * @property string $text
 * @property string $created_at
 * @property string $updated_at
 *
 * @property  $car
 */
class Text extends ActiveRecord implements AssetableInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%text}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'required'],
            [['text'], 'string'],
            [['text'], function ($attribute) {
                $this->$attribute = HtmlPurifier::process($this->$attribute);
            }],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'text' => Yii::t('app', 'Text'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function export()
    {
        return $this->toArray(['id', 'text']);
    }

    /**
     * @inheritdoc
     * @return TextQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TextQuery(get_called_class());
    }
}
