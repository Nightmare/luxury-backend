<?php

namespace app\models;

use app\components\behaviors\UploadImageBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\HtmlPurifier;
use app\models\Query\{
    CarReviewQuery, DomainQuery, GarageQuery, OrderItemQuery, ItemTemplateQuery, CarQuery
};

/**
 * This is the model class for table "{{%car}}".
 *
 * @property integer $id
 * @property integer $domain_id
 * @property string $photo
 * @property integer $price
 * @property string $slug
 * @property string $brand
 * @property string $model
 * @property integer $year
 * @property string $title
 * @property string $text
 * @property integer $priority
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Domain $domain
 * @property CarReview[] $carReviews
 * @property ItemTemplate[] $itemTemplates
 * @property OrderItem[] $orderItems
 * @property Garage[] $garages
 */
class Car extends ActiveRecord implements BillableItemInterface, ComingSoonInterface, ItemTemplateInterface
{

    use BillableItemTrait, AssetableTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%car}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['domain_id', 'brand', 'model', 'year', 'title', 'photo'], 'required', 'on' => ['create']],
            [['domain_id', 'price', 'year', 'priority'], 'integer'],
            [['text', 'title'], function ($attribute) {
                $this->$attribute = HtmlPurifier::process($this->$attribute);
            }],
            [['created_at', 'updated_at', 'slug'], 'safe'],
            [['text', 'meta_keywords', 'meta_description'], 'string'],
            [['title', 'meta_title'], 'string', 'max' => 255],
            [['brand', 'model'], 'string', 'max' => 122],
            [['photo'], 'image', 'extensions' => 'jpg, jpeg, png', 'on' => ['create', 'update']],
            [['domain_id'], 'exist', 'skipOnError' => true, 'targetClass' => Domain::class, 'targetAttribute' => ['domain_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'domain_id' => Yii::t('app', 'Domain ID'),
            'photo' => Yii::t('app', 'Photo'),
            'price' => Yii::t('app', 'Price'),
            'slug' => Yii::t('app', 'Slug'),
            'brand' => Yii::t('app', 'Brand'),
            'model' => Yii::t('app', 'Model'),
            'year' => Yii::t('app', 'Year'),
            'title' => Yii::t('app', 'Title'),
            'text' => Yii::t('app', 'Text'),
            'priority' => Yii::t('app', 'Priority'),
            'meta_title' => Yii::t('app', 'Meta Title'),
            'meta_keywords' => Yii::t('app', 'Meta Keywords'),
            'meta_description' => Yii::t('app', 'Meta Description'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            SluggableBehavior::class => [
                'class' => SluggableBehavior::class,
                'attribute' => ['brand', 'model', 'year'],
                'ensureUnique' => true,
            ],
            [
                'class' => UploadImageBehavior::class,
                'attribute' => 'photo',
                'instanceByName' => true,
                'scenarios' => ['create', 'update'],
                'path' => '@static/uploads/car/{id}/image',
                'url' => '/uploads/car/{id}/image',
                'thumbs' => [],
            ],
        ];
    }

    /**
     * @return DomainQuery|ActiveQuery
     */
    public function getDomain()
    {
        return $this->hasOne(Domain::class, ['id' => 'domain_id']);
    }

    /**
     * @return CarReviewQuery|ActiveQuery
     */
    public function getCarReviews()
    {
        return $this->hasMany(CarReview::class, ['car_id' => 'id']);
    }

    /**
     * @return ItemTemplateQuery|ActiveQuery
     */
    public function getItemTemplates(): ItemTemplateQuery
    {
        return $this->hasMany(ItemTemplate::class, ['id' => 'item_template_id'])
            ->viaTable('{{%car_template}}', ['car_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return OrderItemQuery|ActiveQuery
     */
    public function getOrderItems(): OrderItemQuery
    {
        return $this->hasMany(OrderItem::class, ['car_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return OrderItemQuery|ActiveQuery
     */
    public function getIntersectedOrderItems(): OrderItemQuery
    {
        $query = OrderItem::find()->where(['car_id' => $this->id]);
        if ($garages = $this->getGarages()->select('id')->asArray()->all()) {
            $query->orWhere(['garage_id' => ArrayHelper::getColumn($garages, 'id')]);
        }
        $query->multiple = true;
        return $query;
    }

    /**
     * @return GarageQuery|ActiveQuery
     */
    public function getGarages()
    {
        return $this->hasMany(Garage::class, ['id' => 'garage_id'])->viaTable('garage_cars', ['car_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function isComingSoon(): bool
    {
        return $this->price === null;
    }

    /**
     * @inheritdoc
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        /** @var UploadImageBehavior|self $self */
        $self = $this;
        $fields = parent::fields();
        $fields['photo'] = function () use ($self) {
            return $self->getUploadUrl('photo');
        };

        return $fields;
    }

    /**
     * @inheritdoc
     */
    public function extraFields()
    {
        return array_merge(parent::extraFields(), ['unavailablePeriods', 'assets', 'domain', 'carReviews']);
    }

    /**
     * @inheritdoc
     * @return CarQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CarQuery(get_called_class());
    }

}
