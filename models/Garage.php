<?php

namespace app\models;

use app\components\behaviors\UploadImageBehavior;
use app\models\Query\ItemTemplateQuery;
use app\models\Query\OrderItemQuery;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use app\models\Query\GarageQuery;
use app\models\Query\CarQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%garage}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $title
 * @property string $text
 * @property string $photo
 * @property integer $price
 * @property string $slug
 * @property integer $priority
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property integer $is_active
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Car[] $cars
 * @property OrderItem[] $orderItems
 */
class Garage extends ActiveRecord implements BillableItemInterface
{

    use BillableItemTrait, AssetableTrait;

    /**
     * @var array
     */
    public $car_ids = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%garage}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'photo', 'title', 'car_ids'], 'required', 'on' => ['create']],
            [['price', 'priority', 'is_active'], 'integer'],
            [['created_at', 'updated_at', 'slug'], 'safe'],
            [['name'], 'string', 'max' => 50],
            [['text', 'meta_keywords', 'meta_description'], 'string'],
            [['title', 'meta_title'], 'string', 'max' => 255],
            [['photo'], 'image', 'extensions' => 'jpg, jpeg, png', 'on' => ['create', 'update']],
            ['car_ids', function ($attribute) {
                if ($attribute) {
                    $this->$attribute = trim($this->$attribute, ',');
                    $values = explode(',', $this->$attribute);
                    $idsCount = count($values);
                    if (!$idsCount
                        || count(array_unique($values)) !== $idsCount
                        || Car::find()->where(['id' => $values])->count() != $idsCount
                    ) {
                        $this->addError($attribute, 'Invalid car_ids value.');
                    }
                }
            }]
//            [['car_ids'], 'allowArray' => true, 'exist', 'skipOnError' => true, 'targetClass' => Car::class],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'title' => Yii::t('app', 'Title'),
            'text' => Yii::t('app', 'Text'),
            'photo' => Yii::t('app', 'Photo'),
            'price' => Yii::t('app', 'Price'),
            'slug' => Yii::t('app', 'Slug'),
            'priority' => Yii::t('app', 'Priority'),
            'meta_title' => Yii::t('app', 'Meta Title'),
            'meta_keywords' => Yii::t('app', 'Meta Keywords'),
            'meta_description' => Yii::t('app', 'Meta Description'),
            'is_active' => Yii::t('app', 'Is Active'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            SluggableBehavior::class => [
                'class' => SluggableBehavior::class,
                'attribute' => ['name'],
                'ensureUnique' => true,
            ],
            [
                'class' => UploadImageBehavior::class,
                'attribute' => 'photo',
                'instanceByName' => true,
                'scenarios' => ['create', 'update'],
                'path' => '@static/uploads/garage/{id}',
                'url' => '/uploads/garage/{id}',
                'thumbs' => [],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $db = self::getDb();
        $transaction = $db->beginTransaction();
        try {
            if ($result = parent::save($runValidation, $attributeNames)) {
                $qb = $db->getQueryBuilder();
                $db->createCommand('DELETE FROM `garage_cars` WHERE `garage_id` = :garage_id', [':garage_id' => $this->id])->execute();
                $carIds = is_string($this->car_ids) ? explode(',', $this->car_ids) : $this->car_ids;
                if (count($carIds) > 0) {
                    $rows = [];
                    foreach ($carIds as $carId) {
                        $rows[] = [$this->id, $carId];
                    }
                    $sqlInsert = $qb->batchInsert('garage_cars', ['garage_id', 'car_id'], $rows);
                    $db->createCommand($sqlInsert)->execute();
                }
            }
            $transaction->commit();
            return $result;
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * @inheritdoc
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return CarQuery|ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::class, ['id' => 'car_id'])->viaTable('garage_cars', ['garage_id' => 'id']);
    }

    /**
     * @return ItemTemplateQuery|ActiveQuery
     */
    public function getItemTemplates(): ItemTemplateQuery
    {
        return $this->hasMany(ItemTemplate::class, ['id' => 'item_template_id'])
            ->viaTable('{{%garage_template}}', ['garage_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return OrderItemQuery|ActiveQuery
     */
    public function getOrderItems(): OrderItemQuery
    {
        return $this->hasMany(OrderItem::class, ['garage_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return OrderItemQuery|ActiveQuery
     */
    public function getIntersectedOrderItems(): OrderItemQuery
    {
        $query = OrderItem::find()->where(['garage_id' => $this->id]);
        if ($cars = $this->getCars()->select('id')->asArray()->all()) {
            $query->orWhere(['car_id' => ArrayHelper::getColumn($cars, 'id')]);
        }
        $query->multiple = true;
        return $query;
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        /** @var UploadImageBehavior|self $self */
        $self = $this;
        $fields = parent::fields();
        $fields['photo'] = function () use ($self) {
            return $self->getUploadUrl('photo');
        };

        return $fields;
    }

    /**
     * @inheritdoc
     */
    public function extraFields()
    {
        return array_merge(parent::extraFields(), ['unavailablePeriods', 'cars', 'assets']);
    }

    /**
     * @inheritdoc
     * @return GarageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GarageQuery(get_called_class());
    }
}
