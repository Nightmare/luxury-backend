<?php

namespace app\models;

use app\components\validators\EitherValidator;
use app\models\Query\CarQuery;
use app\models\Query\GarageQuery;
use app\models\Query\OrderQuery;
use Yii;
use yii\db\ActiveRecord;
use app\models\Query\OrderItemQuery;

/**
 * This is the model class for table "{{%order_item}}".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $car_id
 * @property integer $garage_id
 * @property integer $price
 * @property string $status
 * @property string $begins_at
 * @property integer $days_count
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Car $car
 * @property Garage $garage
 * @property Order $order
 */
class OrderItem extends ActiveRecord
{

    const STATUS_FAILED         = 'FAILED';
    const STATUS_COMPLETED      = 'COMPLETED';
    const STATUS_IN_PROGRESS    = 'IN_PROGRESS';
    const STATUS_CANCELLED      = 'CANCELLED';
    const STATUS_EXPIRED        = 'EXPIRED';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_item}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'begins_at', 'price', 'days_count'], 'required', 'on' => 'update'],
            [['car_id', 'order_id', 'price', 'garage_id'], 'integer'],
            [['days_count'], 'integer', 'min' => 1],
            [['status'], 'in', 'range' => static::getStatuses()],
            [['begins_at', 'created_at', 'updated_at'], 'safe'],
//            ['begins_at', 'date', 'format' => 'php:Y-m-d', 'timestampAttribute' => 'begins_at'],
//            [['car_id'], 'unique', 'targetAttribute' => ['car_id', 'status', 'begins_at', 'ends_at'], 'filter' => ['status' => self::STATUS_COMPLETED], 'message' => '.'],
            [['car_id'], 'exist', 'skipOnError' => true, 'targetClass' => Car::class, 'targetAttribute' => ['car_id' => 'id']],
            [['garage_id'], 'exist', 'skipOnError' => true, 'targetClass' => Garage::class, 'targetAttribute' => ['garage_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::class, 'targetAttribute' => ['order_id' => 'id']],
            [['car_id', 'garage_id'], EitherValidator::class, 'on' => ['create']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'price' => Yii::t('app', 'Price'),
            'car_id' => Yii::t('app', 'Car ID'),
            'garage_id' => Yii::t('app', 'Garage ID'),
            'order_id' => Yii::t('app', 'Order ID'),
            'status' => Yii::t('app', 'Status'),
            'begins_at' => Yii::t('app', 'Started At'),
            'days_count' => Yii::t('app', 'Days Count'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return CarQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::class, ['id' => 'car_id']);
    }

    /**
     * @return GarageQuery
     */
    public function getGarage()
    {
        return $this->hasOne(Garage::class, ['id' => 'garage_id']);
    }

    /**
     * @return OrderQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::class, ['id' => 'order_id']);
    }

    /**
     * @inheritdoc
     * @return OrderItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderItemQuery(get_called_class());
    }

    /**
     * @return BillableItemInterface
     */
    public function getBillableItem(): BillableItemInterface
    {
        return ($this->car_id ? $this->getCar() : $this->getGarage())->one();
    }

    /**
     * Returns true if order item is IN PROGRESS status
     *
     * @return bool
     */
    public function isInProgress()
    {
        return $this->status === static::STATUS_IN_PROGRESS;
    }

    /**
     * Returns true if order item can be cancelled
     *
     * @return bool
     */
    public function canBeCancelled()
    {
        return $this->isInProgress();
    }

    /**
     * Returns true if order item can be completed
     *
     * @return bool
     */
    public function canBeCompleted(): bool
    {
        if (!$this->isInProgress()) {
            return false;
        }

        $startsAt = new \DateTime($this->begins_at);
        $endsAt = clone $startsAt;

        if ($this->days_count > 1) {
            $endsAt->modify('+' . ($this->days_count - 1) . 'days');
        }

        return $this->getBillableItem()->isAvailableForPeriod($startsAt, $endsAt);
    }

    /**
     * Returns applicants order items for approve
     *
     * @return OrderItem[]
     */
    public function getApplicantsOrderItemsForApprove(): array
    {
        $startsAt = new \DateTime($this->begins_at);
        $endsAt = clone $startsAt;

        if ($this->days_count > 1) {
            $endsAt->modify('+' . ($this->days_count - 1) . 'days');
        }

        return $this->getBillableItem()->getApplicantsOrderItems($startsAt, $endsAt)->all();
    }

    /**
     * @inheritdoc
     */
    public function extraFields()
    {
        return array_merge(parent::extraFields(), ['car']);
    }

    /**
     * @return array
     */
    public static function getStatuses(): array
    {
        return [self::STATUS_FAILED, self::STATUS_IN_PROGRESS, self::STATUS_COMPLETED, self::STATUS_CANCELLED, self::STATUS_EXPIRED];
    }
}
