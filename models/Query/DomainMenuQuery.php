<?php

namespace app\models\Query;

use yii\db\ActiveQuery;
use app\models\DomainMenu;

/**
 * This is the ActiveQuery class for [[DomainMenu]].
 *
 * @see DomainMenu
 */
class DomainMenuQuery extends ActiveQuery
{

    /**
     * @inheritdoc
     * @return DomainMenu[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return DomainMenu|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
