<?php

namespace app\models\Query;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\app\models\Domain]].
 *
 * @see \app\models\Domain
 */
class DomainQuery extends ActiveQuery
{

    /**
     * @inheritdoc
     * @return \app\models\Domain[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\Domain|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
