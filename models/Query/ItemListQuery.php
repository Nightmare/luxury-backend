<?php

namespace app\models\Query;

use yii\db\ActiveQuery;
use app\models\ItemList;

/**
 * This is the ActiveQuery class for [[ItemList]].
 *
 * @see ItemList
 */
class ItemListQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return ItemList[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ItemList|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
