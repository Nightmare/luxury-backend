<?php

namespace app\models\Query;

use yii\db\ActiveQuery;
use app\models\Garage;

/**
 * This is the ActiveQuery class for [[Garage]].
 *
 * @see Garage
 */
class GarageQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return Garage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Garage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
