<?php

namespace app\models\Query;

use app\models\Order;
use yii\db\{ActiveQuery, Expression};

/**
 * This is the ActiveQuery class for [[\app\models\Order]].
 *
 * @see \app\models\Order
 */
class OrderQuery extends ActiveQuery
{

    /**
     * @return $this|ActiveQuery
     */
    public function uncompleted()
    {
        return $this
            ->alias('o')
            ->select([
                '`o`.`id`',
                '`o`.`user_id`',
                '`u`.`name`',
                '`u`.`phone`',
                new Expression("SUM(`oi`.`price`) as `price`"),
                new Expression("SUM(`oi`.`days_count`) as `days_count`"),
                new Expression("GROUP_CONCAT(CONCAT(`c`.`brand`, ' ', `c`.`model`, ', ', `c`.`year`) SEPARATOR '|') as `cars`"),
                new Expression("GROUP_CONCAT(`g`.`name` SEPARATOR '|') as `corteges`"),
            ])
            ->innerJoinWith('user u', true)
            ->innerJoinWith(['orderItems oi' => function (OrderItemQuery $query) {
                $query->waitingNotExpiredItems()
                    ->leftJoin('car c', '`c`.`id` = `oi`.`car_id`')
                    ->leftJoin('garage g', '`g`.`id` = `oi`.`garage_id`');
            }], true)
            ->groupBy('`o`.`id`');
    }

    /**
     * @return $this
     */
    public function expired()
    {
        return Order::find()->select('[[order]].*')->innerJoinWith(['orderItems' => function (OrderItemQuery $query) {
            $query->waitingExpiredItems();
        }]);
    }

    /**
     * @inheritdoc
     * @return \app\models\Order[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\Order|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}