<?php

namespace app\models\Query;

use app\models\User;
use yii\db\ActiveQuery;
use app\models\CarReview;

/**
 * This is the ActiveQuery class for [[\app\models\CarReview]].
 *
 * @see CarReview
 */
class CarReviewQuery extends ActiveQuery
{

    /**
     * @inheritdoc
     * @return CarReview[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CarReview|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $user = \Yii::$app->getUser()->getIdentity();
        if (!$user || $user->role !== User::ROLE_ADMIN) {
            $this->andWhere(['[[status]]' => CarReview::STATUS_VERIFIED]);
        }
        parent::init();
    }
}
