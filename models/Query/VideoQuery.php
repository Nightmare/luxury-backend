<?php

namespace app\models\Query;

use app\models\Video;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[app\models\Video]].
 *
 * @see Video
 */
class VideoQuery extends ActiveQuery
{

    /**
     * @inheritdoc
     * @return Video[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Video|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}
