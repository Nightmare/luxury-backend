<?php

namespace app\models\Query;

use yii\db\ActiveQuery;
use app\models\OrderItem;

/**
 * This is the ActiveQuery class for [[OrderItem]].
 *
 * @see OrderItem
 */
class OrderItemQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function completedItems()
    {
        return $this->andWhere(['[[status]]' => OrderItem::STATUS_COMPLETED]);
    }

    /**
     * @return $this
     */
    public function waitingNotExpiredItems()
    {
        $today = new \DateTime('now');
        // 10 - if current time greater than 10 PM than order is expired
        $operator = $today->format('h') >= 10 ? '>' : '>=';

        return $this
            ->andWhere(['[[status]]' => OrderItem::STATUS_IN_PROGRESS])
            ->andWhere([$operator, '[[begins_at]]', $today->format('Y-m-d')]);
    }

    /**
     * @return $this
     */
    public function waitingExpiredItems()
    {
        $today = new \DateTime('now');

        // 10 - if current time greater than 10 PM than order is expired
        $operator = $today->format('h') >= 10 ? '<=' : '<';

        return $this
            ->andWhere(['[[status]]' => OrderItem::STATUS_IN_PROGRESS])
            ->andWhere([$operator, '[[begins_at]]', $today->format('Y-m-d')]);
    }

    /**
     * @return $this
     */
    public function activeOrders()
    {
        $today = new \DateTime('today');
        return $this
            ->completedItems()
            ->andWhere('DATE_ADD([[begins_at]], INTERVAL [[days_count]] DAY) >= :date', [':date' => $today->format('Y-m-d')]);
    }

    /**
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     *
     * @return $this
     */
    public function forPeriod($dateFrom, $dateTo)
    {
        return $this
            ->andWhere('[[begins_at]] <= :date_from', [':date_from' => $dateFrom->format('Y-m-d')])
            ->andWhere('DATE_ADD([[begins_at]], INTERVAL [[days_count]] DAY) > :date_to', [':date_to' => $dateTo->format('Y-m-d')]);
    }

    /**
     * @return $this
     */
    public function orderByBeginsDate()
    {
        return $this->orderBy(['[[begins_at]]' => SORT_ASC]);
    }

    /**
     * @return $this
     */
    public function periodsOnly()
    {
        return $this->select(['[[begins_at]], [[days_count]]']);
    }

    /**
     * @inheritdoc
     * @return OrderItem[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return OrderItem|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}