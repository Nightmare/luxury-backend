<?php

namespace app\models\Query;

use app\models\Image;
use app\models\Text;
use app\models\Video;
use yii\db\ActiveQuery;
use app\models\ItemTemplate;

/**
 * This is the ActiveQuery class for ItemTemplate.
 *
 * @see ItemTemplate
 */
class ItemTemplateQuery extends ActiveQuery
{

    /**
     * @return $this
     */
    public function orderBySort()
    {
        return $this->orderBy(['sort' => SORT_ASC]);
    }

    /**
     * @return $this
     */
    public function withAssets()
    {
        $i = Image::tableName();
        $v = Video::tableName();
        $t = Text::tableName();

        return $this
            ->leftJoin($i, "$i.id = type_id AND type = :image_type", ['image_type' => ItemTemplate::TYPE_IMAGE])
            ->leftJoin($v, "$v.id = type_id AND type = :video_type", ['video_type' => ItemTemplate::TYPE_VIDEO])
            ->leftJoin($t, "$t.id = type_id AND type = :text_type", ['text_type' => ItemTemplate::TYPE_TEXT])
            ->where("($i.id IS NOT NULL) OR ($v.id IS NOT NULL) OR ($t.id IS NOT NULL)");
    }

    /**
     * @inheritdoc
     * @return ItemTemplate[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ItemTemplate|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

}
