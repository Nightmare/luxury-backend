<?php

namespace app\models\Query;

use app\models\Domain;
use app\models\User;
use Yii;
use app\models\Car;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Car]].
 *
 * @see Car
 */
class CarQuery extends ActiveQuery
{

    /**
     * @inheritdoc
     * @return Car[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Car|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        /** @var User $user */
        $user = Yii::$app->getUser()->getIdentity();
        if (!$user || $user->role !== User::ROLE_ADMIN) {
            $domainId = -1;
            if ($domain = Domain::findOne(['domain' => preg_replace('/^https?:\/\/(www\.)?/i', '', Yii::$app->getRequest()->getHostInfo())])) {
                $domainId = $domain->getPrimaryKey();
            }
            $this->andWhere(['[[domain_id]]' => $domainId]);
        }
        parent::init();
    }
}
