<?php

namespace app\models\Query;

use app\models\Text;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Text]].
 *
 * @see Text
 */
class TextQuery extends ActiveQuery
{

    /**
     * @inheritdoc
     * @return Text[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Text|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
