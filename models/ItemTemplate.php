<?php

namespace app\models;

use app\components\validators\EitherValidator;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use app\models\Query\{
    GarageQuery, ItemTemplateQuery, ImageQuery, VideoQuery, CarQuery, TextQuery
};

/**
 * This is the model class for table "{{%item_template}}".
 *
 * @property integer $id
 * @property integer $type_id
 * @property string $type
 * @property integer $cols see grid system, max cols is 12 and min is 1
 * @property integer $sort
 * @property string $created_at
 * @property string $updated_at
 */
class ItemTemplate extends ActiveRecord
{
    const TYPE_VIDEO = 'video';
    const TYPE_IMAGE = 'image';
    const TYPE_TEXT  = 'text';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%item_template}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'type', 'sort', 'cols'], 'required'],
            [['type_id', 'sort'], 'integer'],
            [['cols'], 'integer', 'min' => 1, 'max' => 12],
            [['type'], 'string'],
            [['type'], 'in', 'range' => [static::TYPE_VIDEO, static::TYPE_IMAGE, static::TYPE_TEXT]],
            [['created_at', 'updated_at'], 'safe'],
            [['type_id', 'type'], 'unique', 'targetAttribute' => ['type_id', 'type'], 'message' => 'The combination of Type ID and Type has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_id' => Yii::t('app', 'Type ID'),
            'type' => Yii::t('app', 'Type'),
            'cols' => Yii::t('app', 'Columns'),
            'sort' => Yii::t('app', 'Sort'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return AssetableInterface|Image|Text|Video|null
     * @throws \Exception
     */
    public function getAsset()
    {
        switch ($this->type) {
            case static::TYPE_IMAGE:
                $asset = $this->getImage();
                break;

            case static::TYPE_TEXT:
                $asset = $this->getText();
                break;

            case static::TYPE_VIDEO:
                $asset = $this->getVideo();
                break;

            default:
                throw new \Exception('Invalid type: ' . $this->type);
                break;
        }

        return $asset->one();
    }

    /**
     * @param AssetableInterface $asset
     * @return array
     */
    public function exportAsset(AssetableInterface $asset)
    {
        $assetArr = $asset->export() + [
            'type' => $this->type,
//            'sort' => $this->sort,
            'cols' => $this->cols,
        ];
        $assetArr['asset_id'] = $assetArr['id'];
        $assetArr['id'] = $this->id;
        return $assetArr;
    }

    /**
     * @return ImageQuery|ActiveQuery
     */
    private function getImage()
    {
        return $this->hasOne(Image::class, ['id' => 'type_id']);
    }

    /**
     * @return VideoQuery|ActiveQuery
     */
    private function getVideo()
    {
        return $this->hasOne(Video::class, ['id' => 'type_id']);
    }

    /**
     * @return TextQuery|ActiveQuery
     */
    private function getText()
    {
        return $this->hasOne(Text::class, ['id' => 'type_id']);
    }

    /**
     * @inheritdoc
     * @return ItemTemplateQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ItemTemplateQuery(get_called_class());
    }
}
