<?php

namespace app\models;

use app\components\behaviors\UploadImageBehavior;
use app\components\validators\EitherValidator;
use mongosoft\file\UploadBehavior;
use Yii;
use app\models\Query\DomainQuery;
use app\models\Query\ItemQuery;
use yii\behaviors\SluggableBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use app\models\Query\ItemListQuery;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "{{%item_list}}".
 *
 * @property integer $id
 * @property integer $domain_id
 * @property string $slug
 * @property string $title
 * @property string $name
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property integer $is_active
 * @property string $created_at
 * @property string $updated_at
 * @property string $front_image
 * @property string $front_video
 * @property string $front_asset_type
 * @property boolean $show_in_front_page
 *
 * @property Domain $domain
 * @property Item[] $items
 */
class ItemList extends ActiveRecord
{
    const FRONT_ASSET_TYPE_IMAGE = 'image';
    const FRONT_ASSET_TYPE_VIDEO = 'video';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%item_list}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['domain_id', 'front_asset_type', 'title', 'name', 'is_active'], 'required'],
            [['domain_id', 'is_active'], 'integer'],
            [['show_in_front_page'], 'boolean'],
            [['meta_keywords', 'meta_description'], 'string'],
            [['created_at', 'updated_at', 'slug'], 'safe'],
            [['title', 'name', 'meta_title'], 'string', 'max' => 255],
            [['title', 'name'], function ($attribute) {
                $this->$attribute = HtmlPurifier::process($this->$attribute);
            }],
            [['domain_id'], 'exist', 'skipOnError' => true, 'targetClass' => Domain::class, 'targetAttribute' => ['domain_id' => 'id']],
            [['front_image'], 'image', 'extensions' => 'jpg, jpeg, png', 'on' => ['create', 'update']],
            [['front_video'], 'file', 'extensions' => 'mp4, 3gp, mov, m4v, mpeg, mspg', 'on' => ['create', 'update']],
            [['front_image', 'front_video'], EitherValidator::class, 'on' => ['create']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'domain_id' => Yii::t('app', 'Domain ID'),
            'front_image' => Yii::t('app', 'Front Image'),
            'front_video' => Yii::t('app', 'Front Video'),
            'front_asset_type' => Yii::t('app', 'Front Asset Type'),
            'slug' => Yii::t('app', 'Slug'),
            'title' => Yii::t('app', 'Title'),
            'name' => Yii::t('app', 'Name'),
            'meta_title' => Yii::t('app', 'Meta Title'),
            'meta_keywords' => Yii::t('app', 'Meta Keywords'),
            'meta_description' => Yii::t('app', 'Meta Description'),
            'is_active' => Yii::t('app', 'Is Active'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'show_in_front_page' => Yii::t('app', 'Show In Front Page'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            SluggableBehavior::class => [
                'class' => SluggableBehavior::class,
                'attribute' => ['name'],
                'ensureUnique' => true,
            ],
            [
                'class' => UploadImageBehavior::class,
                'attribute' => 'front_image',
                'instanceByName' => true,
                'scenarios' => ['create', 'update'],
                'path' => '@static/uploads/item-list/{id}',
                'url' => '/uploads/item-list/{id}',
                'thumbs' => [],
            ],
            [
                'class' => UploadBehavior::class,
                'attribute' => 'front_video',
                'instanceByName' => true,
                'scenarios' => ['create', 'update'],
                'path' => '@static/uploads/item-list/{id}',
                'url' => '/uploads/item-list/{id}',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return ItemListQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ItemListQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function extraFields()
    {
        return array_merge(parent::extraFields(), ['items', 'domain']);
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        /** @var UploadImageBehavior|self $self */
        $self = $this;
        $fields = parent::fields();
        foreach ($fields as $field) {
            if (in_array($field, ['front_image', 'front_video'], true)) {
                $fields[$field . '_url'] = function () use ($self, $field) {
                    return $self->getUploadUrl($field);
                };
            }
        }

        return $fields;
    }

    /**
     * @return DomainQuery|ActiveQuery
     */
    public function getDomain()
    {
        return $this->hasOne(Domain::class, ['id' => 'domain_id']);
    }

    /**
     * @return ItemQuery|ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Item::class, ['item_list_id' => 'id']);
    }
}
