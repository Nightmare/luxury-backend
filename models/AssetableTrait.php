<?php

namespace app\models;

use app\models\Query\ItemTemplateQuery;

/**
 * Class AssetableTrait
 * @package app\models
 * @method ItemTemplateQuery getItemTemplates()
 */
trait AssetableTrait
{

    /**
     * Returns assets list
     *
     * @return array
     */
    public function getAssets()
    {
        $result = [];
        foreach ($this->getItemTemplates()->withAssets()->orderBySort()->all() as $itemTemplate) {
            $result[] = $itemTemplate->exportAsset($itemTemplate->getAsset());
        }
        return $result;
    }
}