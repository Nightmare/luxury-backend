<?php

namespace app\components\actions;

use Yii;
use yii\db\BaseActiveRecord;
use yii\rest\UpdateAction as BaseUpdateAction;

/**
 * Class UpdateAction
 * @package app\components\actions
 */
class UpdateAction extends BaseUpdateAction
{

    /**
     * @inheritdoc
     */
    public function run($id)
    {
        /** @var BaseActiveRecord $model */
        $model = parent::run($id);
        $model->refresh();
        return $model;
    }
}
