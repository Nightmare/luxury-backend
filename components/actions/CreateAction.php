<?php

namespace app\components\actions;

use Yii;
use yii\db\BaseActiveRecord;
use yii\rest\CreateAction as BaseCreateAction;

/**
 * Class CreateAction
 * @package app\components\actions
 */
class CreateAction extends BaseCreateAction
{
    /**
     * @inheritdoc
     */
    public function run()
    {
        /** @var BaseActiveRecord $model */
        $model = parent::run();
        $model->refresh();
        return $model;
    }
}
