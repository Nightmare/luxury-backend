<?php

namespace app\components\actions;

use Yii;
use yii\db\ActiveRecord;
use yii\db\QueryInterface;

/**
 * Class RestQuery
 * @package app\components\actions
 */
trait RestQuery
{

    /**
     * @param QueryInterface $query
     */
    public function applyQuery(QueryInterface $query)
    {
        /** @var ActiveRecord $model */
        $model = new $query->modelClass;

        $filters = array_diff_key(
            Yii::$app->getRequest()->getQueryParams(),
            array_flip(array_merge(['sort', 'expand', 'fields', 'extraFields', 'page', 'per-page'], $model::primaryKey()))
        );

        foreach ($filters as $filter => $value) {
            $condition = $this->getConditionPart($filter, $value);
            if ($model->hasAttribute($condition[1])) {
                $query->andWhere($condition);
            }
        }
    }

    /**
     * @param string $field
     * @param mixed $value
     *
     * @return array
     */
    private function getConditionPart($field, $value)
    {
        $operator = '=';
        $regexp = '/\s*(>|<|>=|<=|!|!=|<>|%)\s*$/';
        if (preg_match($regexp, $field, $matches)) {
            $field = rtrim(preg_replace($regexp, '', $field));
            $operator = $matches[0];

            if ($operator === '%') {
                $operator = 'like';
            } elseif ($operator === '!') {
                $operator = '!=';
            }
        }

        return [$operator, $field, $value];
    }

}