<?php

namespace app\components\actions;

use Yii;
use yii\rest\IndexAction as BaseIndexAction;

/**
 * Class IndexAction
 * @package app\components\actions
 */
class IndexAction extends BaseIndexAction
{
    use RestQuery;

    /**
     * @inheritdoc
     */
    protected function prepareDataProvider()
    {
        $dataProvider = parent::prepareDataProvider();

        $this->applyQuery($dataProvider->query);

        return $dataProvider;
    }

}