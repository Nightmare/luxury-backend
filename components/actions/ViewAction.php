<?php

namespace app\components\actions;

use yii\behaviors\SluggableBehavior;
use yii\db\BaseActiveRecord;
use yii\rest\ViewAction as BaseViewAction;
use yii\db\ActiveRecordInterface;
use yii\web\NotFoundHttpException;

/**
 * Class ViewAction
 * @package app\components\actions
 */
class ViewAction extends BaseViewAction
{
    use RestQuery;

    /**
     * @inheritdoc
     */
    public function findModel($id)
    {
        if ($this->findModel !== null) {
            return call_user_func($this->findModel, $id, $this);
        }

        /* @var $modelClass ActiveRecordInterface|BaseActiveRecord */
        $modelClass = $this->modelClass;
        $keys = $modelClass::primaryKey();

        if (count($keys) > 1) {
            $values = explode(',', $id);
            if (count($keys) === count($values)) {
                $query = $modelClass::find()->where(array_combine($keys, $values));
            }
        } elseif ($id !== null) {
            $query = $modelClass::find()->where([$keys[0] => $id]);
            if (null !== $behavior = (new $modelClass)->getBehavior(SluggableBehavior::class)) {
                $query->orWhere([$behavior->slugAttribute => $id]);
            }
        }

        if (!isset($query)) {
            throw new NotFoundHttpException("Object not found: $id");
        }

        $this->applyQuery($query);
        $model = $query->one();

        if (!isset($model)) {
            throw new NotFoundHttpException("Object not found: $id");
        }

        return $model;
    }

}