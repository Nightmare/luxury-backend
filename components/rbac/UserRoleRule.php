<?php

namespace app\components\rbac;

use Yii;
use yii\rbac\Rule;
use app\models\User;

/**
 * Class UserRoleRule
 * @package app\components\rbac
 */
class UserRoleRule extends Rule
{

    /**
     * @inheritdoc
     */
    public $name = 'userRole';

    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        if (!Yii::$app->getUser()->getIsGuest()) {
            $role = Yii::$app->getUser()->getIdentity()->role;
            if ($item->name === User::ROLE_ADMIN) {
                return $role === User::ROLE_ADMIN;
            } elseif ($item->name === User::ROLE_USER) {
                return $role === User::ROLE_USER || $role === User::ROLE_ADMIN;
            }
        }
        return true;
    }

}