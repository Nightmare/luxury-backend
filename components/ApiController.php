<?php

namespace app\components;

use Yii;
use app\models\User;
use app\components\actions;
use yii\filters\auth\{
    CompositeAuth,
    HttpBearerAuth,
    QueryParamAuth
};
use yii\rest\{
    ActiveController,
    Serializer
};
use yii\filters\{ContentNegotiator,AccessControl};
use yii\web\ForbiddenHttpException;
use yii\web\Response;

/**
 * Class ApiController
 * @package app\controllers\v1
 */
abstract class ApiController extends ActiveController
{

//    /**
//     * @inheritdoc
//     */
//    public $updateScenario = 'update';
//
//    /**
//     * @inheritdoc
//     */
//    public $createScenario = 'create';

    /**
     * @inheritdoc
     */
    public $serializer = [
        'class' => Serializer::class,
        'collectionEnvelope' => 'items',
    ];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::class,
            'formats' => [
//                'application/json' => YII_DEBUG ? Response::FORMAT_JSON : 'jsonvp',
                'application/json' => Response::FORMAT_JSON,
            ],
        ];

        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();

        $actions['index']['class'] = actions\IndexAction::class;
        $actions['view']['class'] = actions\ViewAction::class;
        $actions['create']['class'] = actions\CreateAction::class;
        $actions['update']['class'] = actions\UpdateAction::class;

        return $actions;
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (!Yii::$app->getUser()->can($action->id)) {
                throw new ForbiddenHttpException('Access denied');
            }
            return true;
        } else {
            return false;
        }
    }
}