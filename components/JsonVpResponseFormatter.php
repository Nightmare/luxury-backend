<?php

namespace app\components;

use yii\helpers\Json;
use yii\web\Response;
use yii\web\ResponseFormatterInterface;

/**
 * Class JsonVpResponseFormatter
 * @package app\components
 */
class JsonVpResponseFormatter implements ResponseFormatterInterface
{

    /**
     * Format as Vulnerability Protected JSON.
     * @param Response $response
     */
    public function format($response)
    {
        $response->getHeaders()->set('Content-Type', 'application/json; charset=UTF-8');
        if ($response->data !== null) {
            $response->content = ")]}',\n" . Json::encode($response->data);
        }
    }

}