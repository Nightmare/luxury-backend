<?php

namespace app\components\behaviors;

use Imagine\Image\Box;
use Imagine\Image\Point;
use yii\db\BaseActiveRecord;
use yii\imagine\Image;
use mongosoft\file\UploadImageBehavior as BaseUploadImageBehavior;

/**
 * Class UploadImageBehavior
 * @package app\components\behaviors
 */
class UploadImageBehavior extends BaseUploadImageBehavior
{

    /**
     * @var array|null
     */
    public $cropAreaBoundsAttribute;

    /**
     * @inheritdoc
     */
    protected function save($file, $path)
    {
        /** @var BaseActiveRecord $model */
        $model = $this->owner;
        if ($this->cropAreaBoundsAttribute) {
            $attribute = $model->{$this->cropAreaBoundsAttribute};
            if (is_array($attribute) && count($attribute) > 0) {
                $image = Image::getImagine()->open($file->tempName);

                try {
                    $image
                        ->crop(
                            new Point($attribute['left'], $image->getSize()->getHeight() - $attribute['top']),
                            new Box($attribute['right'] - $attribute['left'], $attribute['top'] - $attribute['bottom'])
                        )
                        ->save($path, ['quality' => 100]);
                    return true;
                } catch (\Exception $e) {
                    return false;
                } finally {
                    if ($this->deleteTempFile) {
                        @unlink($file->tempName);
                    }
                }
            }
        }

        return $file->saveAs($path, $this->deleteTempFile);
    }

}