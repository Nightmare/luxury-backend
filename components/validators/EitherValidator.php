<?php

namespace app\components\validators;

use yii\validators\Validator;

/**
 * Class EitherValidator
 * @package app\components\validators
 */
class EitherValidator extends Validator
{

    /**
     * @inheritdoc
     */
    public function validateAttributes($model, $attributes = null)
    {
        $values = $labels = [];
        $attributes = $this->attributes;
        foreach ($attributes as $attribute) {
            $labels[] = $model->getAttributeLabel($attribute);
            if (!empty($model->$attribute)) {
                $values[] = $model->$attribute;
            }
        }

        if (empty($values)) {
            $labels = implode('", "', $labels);
            foreach ($attributes as $attribute) {
                $this->addError($model, $attribute, "One of \"{$labels}\" attributes cannot be blank.");
            }
            return false;
        }
        return true;
    }
}
